<?php

namespace Bairwell\NameAlternatives\Tests;

/**
 *
 */
abstract class Base extends \PHPUnit_Framework_TestCase
{


    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    protected function runOverviewTest($libraryName)
    {
        $this->object = \Bairwell\DI::getLibrary($libraryName);
        $this->assertTrue($this->object instanceof \Bairwell\NameAlternatives\OverviewAbstract);
        $modules = $this->object->getModules();
        $ascii = $this->getMock('\Bairwell\NameAlternatives\Utilities\Ascii');
        $ascii->expects($this->once())
            ->method('parse')
            ->with('abc')
            ->will($this->returnValue(Array('ABC')));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\Ascii', $ascii);

        $callBack = function($name)
        {
            return Array($name . 'X');
        };
        $counter = 0;
        foreach ($modules as $module) {
            $mock = $this->getMock($module);
            $callback = function($name) use ($module)
            {
                return Array($module);
            };
            $mock->expects($this->exactly(++$counter))
                ->method('parse')
                ->with($this->anything())
                ->will($this->returnCallback($callback));
            \Bairwell\DI::addLibrary($module, $mock);
        }

        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        \Bairwell\DI::mocksOnly(TRUE);

        $expect = Array('ABC');
        foreach ($modules as $module) {
            $expect[] = $module;
        }
        $expectedChanges = Array('abc' => $expect);
        $this->runNamesTest($expectedChanges, 'parse');
    }

    protected function runNamesTest($expectedChanges)
    {
        foreach ($this->names as $name) {
            $results = $this->object->parse($name);
            $this->assertTrue(is_array($results), 'When checking ' . $name . ' did not get array in return:' . gettype($results));
            $this->assertFalse(in_array($name, $results), 'Got back original ' . $name . ' when searching');
            if (isset($expectedChanges[$name]) === TRUE) {
                if (is_array($expectedChanges[$name]) === TRUE) {
                    $resultsCount = count($results);
                    $changesCount = count($expectedChanges[$name]);
                    $imploded = implode(',', $results);
                    if ($resultsCount !== $changesCount) {
                        if ($resultsCount > $changesCount) {
                            $differences = 'Additional: ' . implode(',', array_diff($results, $expectedChanges[$name]));
                        } else {
                            $differences = 'Missing: ' . implode(',', array_diff($expectedChanges[$name], $results));
                        }
                        $this->assertEquals(
                            $changesCount, $resultsCount,
                            'When checking ' . $name . ' got ' . $imploded . ' (' . $differences . ')'
                        );
                    } else {
                        $this->assertEmpty(
                            array_diff($results, $expectedChanges[$name]),
                            'When checking ' . $name . ' got ' . $imploded
                        );
                    }
                    //$this->assertSame($expectedChanges[$name], $results, 'When checking ' . $name . ' for ' . $function . ' got ' . $imploded);
                } else {
                    if (is_string($expectedChanges[$name]) === TRUE) {
                        $count = count($results);
                        $imploded = '';
                        if ($count > 0) {
                            $imploded = implode('*,*', $results);
                        }
                        $this->assertEquals(
                            1, $count,
                            'When checking ' . $name . ' expected 1 result, got : ' . $count . $imploded
                        );
                        $this->assertEquals(
                            $expectedChanges[$name], array_pop($results), 'When checking ' . $name
                        );
                    } else {
                        throw new \Exception('Unrecognised check');
                    }
                }
            } else {
                $count = count($results);
                if ($count > 0) {
                    if ($count === 1) {
                        $this->fail('Changed ' . $name . ' to ' . $results[0]);
                    } else {
                        $imploded = implode(',', $results);
                        $this->fail(
                            'Returned ' . count($results) . ' when processing ' . $name . ' : ' . $imploded
                        );
                    }
                }
            }
        }
    }

    protected function setupMockCommentedCSV($filesToNames)
    {
        $commentedCSVMock = $this->getMock(
            '\Bairwell\NameAlternatives\Tests\Fixtures\CommentedStub',
            array('searchForCSVmatches')
        );
        $callBack = function($filename, $name) use ($filesToNames)
        {
            $matchedName = NULL;
            foreach ($filesToNames as $thisfilename => $data) {
                if (mb_substr($filename, -(mb_strlen($thisfilename))) === $thisfilename) {
                    $matchedName = $thisfilename;
                }
            }
            if ($matchedName === NULL) {
                throw new \Exception('Unrecognised file name ' . $filename . ' for name ' . $name);
            }
            $return = Array();
            foreach ($filesToNames[$matchedName] as $namearray) {
                if (in_array($name, $namearray) === TRUE) {
                    foreach ($namearray as $newname) {
                        if ($newname !== $name) {
                            $return[] = $newname;
                        }
                    }
                }
            }
            return $return;
        };
        $commentedCSVMock->expects($this->any())
            ->method('searchForCSVmatches')
            ->with($this->anything(), $this->anything(), $this->anything())
            ->will($this->returnCallback($callBack));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\CommentedCSV', $commentedCSVMock);
    }


}