<?php
/**
 * Tests the Ascii functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the Ascii functions
 * @throws \Exception
 */
class AsciiTest extends \PHPUnit_Framework_TestCase
{

    protected $names = Array('SIÂN', 'GAIL', 'THÉRÈSE', 'O\'NEILL', 'ÖPIK');

    /**
     * @var \Bairwell\NameAlternatives\GB\Forenames $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\Ascii');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    /**
     * Tests the null string return
     * @throws \Exception
     * @return void
     */
    public function testEmpty()
    {
        $result = $this->object->parse('');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('', $result[0]);
    }

    /**
     * Tests the unprintable string return
     * @throws \Exception
     * @return void
     */
    public function testUnprintable()
    {
        $result = $this->object->parse(chr(79));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('O', $result[0]);
    }

    /**
     * Tests the  ISO functions
     * @throws \Exception
     * @return void
     */
    public function testISO()
    {
        $result = $this->object->parse(chr(128) . chr(131) . chr(142));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('EFZ', $result[0]);
    }

    /**
     * Tests the UTF8
     * @throws \Exception
     * @return void
     */
    public function testUTF8()
    {
        $result = $this->object->parse('SIÂN');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'SIAN');

        $result = $this->object->parse('THÉRÈSE');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'THERESE');

        $result = $this->object->parse('O\'NEILL');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'ONEILL');

        $result = $this->object->parse('ÖPIK');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'OEPIK');
    }

    /**
     * Tests utf8 characters
     * @covers \Bairwell\NameAlternatives\Utilities\Ascii::getStepLevel
     */
    public function testUTF_step2()
    {
        $result = $this->object->parse(chr(0xEE) . chr(0xF8));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('IO', $result[0]);
    }

    /**
     * Tests utf8 characters
     * @covers \Bairwell\NameAlternatives\Utilities\Ascii::getStepLevel
     */
    public function testUTF_step3()
    {
        $result = $this->object->parse(chr(0xF0) . chr(0xF8));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('DHO', $result[0]);
    }

    /**
     * Tests utf8 characters
     * @covers \Bairwell\NameAlternatives\Utilities\Ascii::getStepLevel
     */
    public function testUTF_step4()
    {
        $result = $this->object->parse(chr(0xF8) . chr(0xF8));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('OO', $result[0]);
    }

    /**
     * Tests utf8 characters
     * @covers \Bairwell\NameAlternatives\Utilities\Ascii::getStepLevel
     */
    public function testUTF_step5()
    {
        $result = $this->object->parse(chr(0xFC) . chr(0xF8));
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('UO', $result[0]);
    }

    /**
     * Tests utf8 characters
     * @covers \Bairwell\NameAlternatives\Utilities\Ascii::seemsUtf8
     */
    public function testUTF_FalseEnd()
    {
        $result = $this->object->parse(chr(238) . chr(248));
    }

    /**
     * From https://github.com/doctrine/doctrine1/blob/master/tests/Ticket/2160TestCase.php
     * testGermanCharactersAreConvertedCorrectly
     *
     * @return void
     */
    public function testUTF_German()
    {
        $result = $this->object->parse('Ästhetik');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'AESTHETIK');

        $result = $this->object->parse('ästhetisch');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'AESTHETISCH');

        $result = $this->object->parse('Übung');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'UEBUNG');

        $result = $this->object->parse('über');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'UEBER');

        $result = $this->object->parse('Öl');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'OEL');

        $result = $this->object->parse('ölig');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'OELIG');

        $result = $this->object->parse('Fuß');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'FUSS');

    }

    public function testStrip()
    {
        $result = $this->object->parse('AB C123!"£$%^&*()_=+[]{}#~\'@,<>./');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals($result[0], 'AB C&+');
    }

}