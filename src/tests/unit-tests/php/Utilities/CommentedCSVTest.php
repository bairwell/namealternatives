<?php
/**
 * Tests the Utilities code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\Utilities;

/**
 * Test class for CommentedCSV
 */
class CommentedCSVTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var \Bairwell\NameAlternatives\Utilities\CommentedCSV $object
     */
    protected $object;

    /**
     * The CommentedCSV library, but with the file checks removed
     * @var \Bairwell\NameAlternatives\Utilities\CommentedCSV $object
     */
    protected $noChecksObject;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        if (class_exists('\vfsStreamWrapper', TRUE) === FALSE) {
            $this->markTestSkipped(
                'File system not mockable. See http://www.phpunit.de/manual/3.6/en/test-doubles.html#test-doubles.mocking-the-filesystem'
            );
            return;
        }
        $root = \vfsStream::setup('exampleDir');
        \vfsStream::newFile('unreadable.csv', 0000)->at($root);
        \vfsStream::newFile('unopenable.csv', 0000)->at($root);
        $this->csvFile = \vfsStream::newFile('test.csv')->at($root);
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\CommentedCSV');

        \Bairwell\DI::mocksOnly(TRUE);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    /**
     * @return void
     */
    public function testLoadCommentedFileMustBeAString()
    {
        $emess = NULL;
        try {
            $this->object->loadCommentedCSV(123);
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Filename must be a string', $emess);
    }

    /**
     * @return void
     */
    public function testLoadCommentedFilenameCannotBeEmpty()
    {
        $emess = NULL;
        try {
            $this->object->loadCommentedCSV('');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Filename cannot be empty', $emess);
    }

    /**
     * @return void
     */
    public function testLoadCommentedUnableToFind()
    {
        $emess = NULL;
        try {
            $this->object->loadCommentedCSV('Fred');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Unable to find file Fred', $emess);
    }

    /**
     * @return void
     */
    public function testLoadCommentedMustBeAFile()
    {
        $emess = NULL;
        try {
            $this->object->loadCommentedCSV(\vfsStream::url('exampleDir/'));
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('vfs://exampleDir/ is not a file', $emess);
    }

    /**
     * @return void
     */
    public function testLoadCommentedUnableToRead()
    {
        $emess = NULL;
        try {
            $this->object->loadCommentedCSV(\vfsStream::url('exampleDir/unreadable.csv'));
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Unable to read file vfs://exampleDir/unreadable.csv', $emess);
    }

    /**
     * @return void
     */
    public function testLoadCommentedUnableToOpen()
    {
        $mock = $this->getMock('\Bairwell\NameAlternatives\Utilities\CommentedCSV', Array('checkFile'));
        $mock->expects($this->once())
            ->method('checkFile')
            ->will($this->returnValue(TRUE));
        $emess = NULL;
        try {
            $mock->loadCommentedCSV('XXX');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('fopen(XXX): failed to open stream: No such file or directory', $emess);
    }

    public function testSearchForCSVMatches_Simple()
    {
        $this->csvFile->setContent('RICHARD,RICHY,RICHIE,HELLO' . "\n" .
            'FARMER,LISTER,SHEEP' . "\n" .
            'M&MS,M AND MS,M AND M,M+M');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'HELLO');
        $this->assertTrue(is_array($results));
        $this->assertEquals($results, Array('RICHARD', 'RICHY', 'RICHIE'));
    }

    public function testSearchForCSVMatches_SimpleShouldNotMatchMidword()
    {
        $this->csvFile->setContent('RICHARD,RICHY,RICHIE,HELLO' . "\n" .
            'FARMER,LISTER,SHEEP' . "\n" .
            'M&MS,M AND MS,M AND M,M+M');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'SHEEPSMITH');
        $this->assertTrue(is_array($results));
        $this->assertEquals(Array(), $results);
    }

    public function testSearchForCSVMatches_Mulitword()
    {
        $this->csvFile->setContent('M&MS,M AND MS,M AND M,M+M');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'M&MS');
        $this->assertTrue(is_array($results));
        $this->assertEquals($results, Array('M AND MS', 'M AND M', 'M+M'));
        $results = $this->object->searchForCSVmatches($url, 'M AND M');
        $this->assertTrue(is_array($results));
        $this->assertEquals($results, Array('M&MS', 'M AND MS', 'M+M'));

    }

    public function testSearchForCSVMatches_MulitwordWithinText()
    {
        $this->csvFile->setContent('M&MS,M AND MS,M AND M,M+M');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'I LIKE EATING M&MS SWEETS MADE BY M+MINC');
        $this->assertTrue(is_array($results));
        $expected = Array('I LIKE EATING M AND MS SWEETS MADE BY M+MINC', 'I LIKE EATING M AND M SWEETS MADE BY M+MINC', 'I LIKE EATING M+M SWEETS MADE BY M+MINC');
        $this->assertEquals($expected, $results);
        $results = $this->object->searchForCSVmatches($url, 'I LIKE EATING M AND M SWEETS MADE BY M+MINC CORP');
        $this->assertTrue(is_array($results));
        $expected = Array('I LIKE EATING M&MS SWEETS MADE BY M+MINC CORP', 'I LIKE EATING M AND MS SWEETS MADE BY M+MINC CORP', 'I LIKE EATING M+M SWEETS MADE BY M+MINC CORP');
        $this->assertEquals($expected, $results);
    }

    public function testSearchForCSVMatches_SingleWordWithinText()
    {
        $this->csvFile->setContent('RICK,RIC,RICHARD' . "\n" . 'ROD,RODERICK,RIC');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'RICHARD');
        $this->assertTrue(is_array($results));
        $expected = Array('RICK', 'RIC');
        $this->assertEquals($expected, $results);
        /**
         * Should not match Roderick
         */
        $results = $this->object->searchForCSVmatches($url, 'RICK');
        $this->assertTrue(is_array($results));
        $expected = Array('RIC', 'RICHARD');
        $this->assertEquals($expected, $results);
    }

    /**
     * Should not match DICKEN to KEN
     */
    public function testSearchForCSVMatches_PartialWordMatch()
    {
        $this->csvFile->setContent('KEN,JENNETH');
        $url = \vfsStream::url('test.csv');
        $results = $this->object->searchForCSVmatches($url, 'DICKEN');
        $this->assertTrue(is_array($results));
        $expected = Array();
        $this->assertEquals($expected, $results);
    }
}