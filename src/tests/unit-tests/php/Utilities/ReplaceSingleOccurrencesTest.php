<?php
/**
 * Tests the Replace Single Occurrences functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the replace single occurrences
 * @throws \Exception
 */
class ReplaceSingleOccurrencesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\ReplaceSingleOccurrences $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ReplaceSingleOccurrences');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testExceptionName()
    {
        $emess = NULL;
        try {
            $results = $this->object->replace(123, '', '');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Name must be a string', $emess);
    }

    public function testExceptionSearch()
    {
        $emess = NULL;
        try {
            $results = $this->object->replace('', 123, '');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Search must be a string', $emess);
    }

    public function testExceptionReplace()
    {
        $emess = NULL;
        try {
            $results = $this->object->replace('', '', 123);
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Replace must be a string', $emess);
    }

    public function testReplace()
    {
        $result = $this->object->replace('AABBCCDDAAABBBCCC', 'B', 'X');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('AAXBCCDDAAABBBCCC', 'AABXCCDDAAABBBCCC',
            'AABBCCDDAAAXBBCCC', 'AABBCCDDAAABXBCCC', 'AABBCCDDAAABBXCCC', 'AAXXCCDDAAAXXXCCC');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }


}