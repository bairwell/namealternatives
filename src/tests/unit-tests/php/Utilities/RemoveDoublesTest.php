<?php
/**
 * Tests the Remove Doubles functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the remove doubles
 * @throws \Exception
 */
class RemoveDoublesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\RemoveDoubles $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\RemoveDoubles');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testRemoveDoublesNotString()
    {
        $result = $this->object->parse(123);
        $this->assertTrue(is_array($result));
        $this->assertEquals(0, count($result));
    }

    public function testRemoveDoubles_NoString()
    {
        $result = $this->object->parse('');
        $this->assertTrue(is_array($result));
        $this->assertEquals(0, count($result));
    }

    public function testRemoveDoubles_NoDoubles()
    {
        $result = $this->object->parse('ABCDEFG');
        $this->assertTrue(is_array($result));
        $this->assertEquals(0, count($result));
    }

    public function testRemoveDoubles_SingleDouble()
    {
        $result = $this->object->parse('ABBCDEFG');
        $this->assertTrue(is_array($result));
        $this->assertEquals(1, count($result));
        $this->assertEquals('ABCDEFG', $result[0]);
    }

    public function testRemoveDoubles_TwoDoubles()
    {
        $result = $this->object->parse('ABBCDEEFG');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('ABBCDEFG', 'ABCDEFG', 'ABCDEEFG');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testRemoveDoubles_Tripled()
    {
        $result = $this->object->parse('ABCDEEE');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('ABCDEE', 'ABCDE');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testRemoveDoubles_Complex()
    {
        $result = $this->object->parse('AABCDEEEFF');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array(
            'AABCDEEEF', 'AABCDEEF', 'AABCDEF', 'ABCDEF', 'ABCDEEF', 'ABCDEEEF', 'AABCDEEFF', 'AABCDEFF', 'ABCDEFF',
            'ABCDEEFF', 'ABCDEEEFF'
        );
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }
}