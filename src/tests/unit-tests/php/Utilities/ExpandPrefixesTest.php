<?php
/**
 * Tests the expands prefixes functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the expand ands functions
 * @throws \Exception
 */
class ExpandPrefixesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\ExpandPrefixes $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ExpandPrefixes');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testExpandExceptionPrefixString()
    {
        $emess = NULL;
        try {
            $results = $this->object->expand(123, 'DR SAM BAIRSTOW', Array('DOCTOR', 'PROF', 'PROFESSOR'), Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Prefix needs to be a string', $emess);
    }

    public function testExpandExceptionNameString()
    {
        $emess = NULL;
        try {
            $results = $this->object->expand('ABC', 123, Array('DOCTOR', 'PROF', 'PROFESSOR'), Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Name needs to be a string', $emess);
    }

    public function testExpandExceptionAlternativesArray()
    {
        $emess = NULL;
        try {
            $results = $this->object->expand('ABC', 'TEST', 123, Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Alternatives needs to be an array', $emess);
    }

    public function testExpandPrefixMatch()
    {
        $result = $this->object->expand('MC', 'MCDONALDS', Array('MAC', 'MIC', 'MIK'), Array());
        $this->assertTrue(is_array($result));
        $expectedMatches = Array(
            'MACDONALDS', 'MICDONALDS', 'MIKDONALDS'
        );
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testExpandAbbreviationsMatch()
    {
        $result = $this->object->expand('MC', 'MACDONALDS', Array('MAC', 'MIC', 'MIK'), Array());
        $this->assertTrue(is_array($result));
        $expectedMatches = Array(
            'MCDONALDS', 'MICDONALDS', 'MIKDONALDS'
        );
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

}

