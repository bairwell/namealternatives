<?php
/**
 * Tests the expands abbreviations functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the expand abbreviations functions
 * @throws \Exception
 */
class ExpandAbbreviationsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\ExpandAbbreviations $object
     */
    protected $object;


    /**
     * @var array A list of simple 1<>1 abbreviations
     */
    protected $simpleAbbreviations = Array(
        'NASA' => 'NATIONAL AERONAUTICS AND SPACE ADMINISTRATION',
        'BBC' => 'BRITISH BROADCASTING CORPORATION',
        'UFO' => 'UNIDENTIFIED FLYING OBJECT',
        'ACME' => 'ACME COMPANY MAKES EVERYTHING', // recursive
        'HURD' => 'HIRD OF UNIX-REPLACING DAEMONS',
        'HIRD' => 'HURD OF INTERFACES REPRESENTING DEPTH'
    );

    protected $multipleAbbreviations = Array(
        'RAID' => Array('REDUNDANT ARRAY OF INDEPENDENT DISKS', 'REDUNDANT ARRAY OF INEXPENSIVE DISKS'),
        'TLA' => Array('THREE LETTER ACRONYM', 'THREE LETTER ABBREVIATION'),
        'CSS' => Array('CASCADING STYLE SHEETS', 'CONTENT-SCRAMBLING SYSTEM', 'CLOSED SOURCE SOFTWARE', 'CROSS-SITE SCRIPTING'),
        'CBT' => Array('COMPULSORY BASIC TRAINING', 'COGNITIVE BEHAVIORAL THEORY', 'COMPUTER BASED TRAINING', 'CURRENT BIG THING')
    );

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ExpandAbbreviations');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testExceptionString()
    {
        $emess = NULL;
        try {
            $results = $this->object->expand(123, Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Original name needs to be a string', $emess);
    }


    public function testExceptionAlternativesArray()
    {
        $emess = NULL;
        try {
            $results = $this->object->expand('ABC', 'TEST');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Abbreviations needs to be an array', $emess);
    }

    /**
     * Text at end
     */
    public function testSingleMeaningsWordAtEnd()
    {

        $result = $this->object->expand('SPACESHIPS BY NASA', $this->simpleAbbreviations);
        $expectedMatches = Array('SPACESHIPS BY NATIONAL AERONAUTICS AND SPACE ADMINISTRATION');
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    /**
     * Text in the middle
     */
    public function testSingleMeaningsWordInMiddle()
    {
        $result = $this->object->expand('TELEVISION SERVICES BY THE BBC AT WOOD LANE', $this->simpleAbbreviations);
        $expectedMatches = Array('TELEVISION SERVICES BY THE BRITISH BROADCASTING CORPORATION AT WOOD LANE');
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    /**
     * Text at the start
     */
    public function testSingleMeaningsWordAtStart()
    {
        $result = $this->object->expand('UFO SPOTTED', $this->simpleAbbreviations);
        $expectedMatches = Array('UNIDENTIFIED FLYING OBJECT SPOTTED');
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    /**
     * Text in mid-word should not be replaced
     */
    public function testMidwordShouldNotChange()
    {
        $result = $this->object->expand('UFOS SPOTTED', $this->simpleAbbreviations);
        $expectedMatches = Array();
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    /**
     * This should run promptly with a single result
     */
    public function testRecursion()
    {
        $result = $this->object->expand('ACME', $this->simpleAbbreviations);
        $expectedMatches = Array('ACME COMPANY MAKES EVERYTHING');
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testSingleMeaningsMultipleEntries()
    {
        $result = $this->object->expand('BBC REPORTS THAT NASA HAS SPOTTED A UFO', $this->simpleAbbreviations);
        $expectedMatches = Array(
            'BBC REPORTS THAT NATIONAL AERONAUTICS AND SPACE ADMINISTRATION HAS SPOTTED A UFO',
            'BRITISH BROADCASTING CORPORATION REPORTS THAT NASA HAS SPOTTED A UFO',
            'BRITISH BROADCASTING CORPORATION REPORTS THAT NATIONAL AERONAUTICS AND SPACE ADMINISTRATION HAS SPOTTED A UFO',
            'BBC REPORTS THAT NASA HAS SPOTTED A UNIDENTIFIED FLYING OBJECT',
            'BBC REPORTS THAT NATIONAL AERONAUTICS AND SPACE ADMINISTRATION HAS SPOTTED A UNIDENTIFIED FLYING OBJECT',
            'BRITISH BROADCASTING CORPORATION REPORTS THAT NASA HAS SPOTTED A UNIDENTIFIED FLYING OBJECT',
            'BRITISH BROADCASTING CORPORATION REPORTS THAT NATIONAL AERONAUTICS AND SPACE ADMINISTRATION HAS SPOTTED A UNIDENTIFIED FLYING OBJECT'
        );
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testMultipleMeanings()
    {
        $result = $this->object->expand('I HAVE JUST COMPLETED CBT', $this->multipleAbbreviations);
        $expectedMatches = Array(
            'I HAVE JUST COMPLETED COMPULSORY BASIC TRAINING',
            'I HAVE JUST COMPLETED COGNITIVE BEHAVIORAL THEORY',
            'I HAVE JUST COMPLETED COMPUTER BASED TRAINING',
            'I HAVE JUST COMPLETED CURRENT BIG THING'
        );
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testMultipleMeaningsMultipleTimes()
    {
        $result = $this->object->expand('THE TLA OF CBT CAN BE STORED ON RAID', $this->multipleAbbreviations);

        $expectedMatches = Array(
            'THE TLA OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE TLA OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ACRONYM OF CBT CAN BE STORED ON RAID',
            'THE THREE LETTER ABBREVIATION OF CBT CAN BE STORED ON RAID',
            'THE THREE LETTER ACRONYM OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ABBREVIATION OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ACRONYM OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ABBREVIATION OF CBT CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE TLA OF COMPULSORY BASIC TRAINING CAN BE STORED ON RAID',
            'THE TLA OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON RAID',
            'THE TLA OF COMPUTER BASED TRAINING CAN BE STORED ON RAID',
            'THE TLA OF CURRENT BIG THING CAN BE STORED ON RAID',
            'THE TLA OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE TLA OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE TLA OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE TLA OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE TLA OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE TLA OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE TLA OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE TLA OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ACRONYM OF COMPULSORY BASIC TRAINING CAN BE STORED ON RAID',
            'THE THREE LETTER ACRONYM OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON RAID',
            'THE THREE LETTER ACRONYM OF COMPUTER BASED TRAINING CAN BE STORED ON RAID',
            'THE THREE LETTER ACRONYM OF CURRENT BIG THING CAN BE STORED ON RAID',
            'THE THREE LETTER ABBREVIATION OF COMPULSORY BASIC TRAINING CAN BE STORED ON RAID',
            'THE THREE LETTER ABBREVIATION OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON RAID',
            'THE THREE LETTER ABBREVIATION OF COMPUTER BASED TRAINING CAN BE STORED ON RAID',
            'THE THREE LETTER ABBREVIATION OF CURRENT BIG THING CAN BE STORED ON RAID',
            'THE THREE LETTER ACRONYM OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ACRONYM OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ACRONYM OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ACRONYM OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ABBREVIATION OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ABBREVIATION OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ABBREVIATION OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ABBREVIATION OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INDEPENDENT DISKS',
            'THE THREE LETTER ACRONYM OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ACRONYM OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ACRONYM OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ACRONYM OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ABBREVIATION OF COMPULSORY BASIC TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ABBREVIATION OF COGNITIVE BEHAVIORAL THEORY CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ABBREVIATION OF COMPUTER BASED TRAINING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS',
            'THE THREE LETTER ABBREVIATION OF CURRENT BIG THING CAN BE STORED ON REDUNDANT ARRAY OF INEXPENSIVE DISKS'
        );
        $this->assertTrue(is_array($result));
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    /**
     * Tests if you can match in reverse
     */
    public function testInReverse()
    {
        $result = $this->object->expand('THE DOG TEST', Array(
            'CAT' => 'DOG'
        ));
        $this->assertTrue(is_array($result));
        $this->assertEquals(count(1), count($result));
        $this->assertEquals('THE CAT TEST', $result[1]);
    }
}

