<?php
/**
 * Tests the Remove Common Words functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the remove common words
 * @throws \Exception
 */
class RemovePrefixesOrSuffixesTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testPrefixExceptionString()
    {
        $emess = NULL;
        try {
            $results = $this->object->removePrefixes(123, Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Name must be a string', $emess);
    }

    public function testSuffixExceptionString()
    {
        $emess = NULL;
        try {
            $results = $this->object->removeSuffixes(123, Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Name must be a string', $emess);
    }

    public function testPrefixExceptionList()
    {
        $emess = NULL;
        try {
            $results = $this->object->removePrefixes('test', 'hello');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Exts must be an array', $emess);
    }

    public function testSuffixExceptionList()
    {
        $emess = NULL;
        try {
            $results = $this->object->removeSuffixes('test', 'hello');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Exts must be an array', $emess);
    }


    public function testPrefixes()
    {
        $result = $this->object->removePrefixes('MR SMITH MR', Array('MR', 'MRS'));
        $this->assertTrue(is_array($result));

        $expectedMatches = Array('SMITH MR');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testPrefixesDoNotRemoveEnd()
    {
        $result = $this->object->removePrefixes('MR SMITH AC', Array('AC', 'ACDC'));
        $this->assertTrue(is_array($result));

        $expectedMatches = Array();
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }


    public function testSuffixesDoNotRemovePrefix()
    {
        $result = $this->object->removeSuffixes('MR SMITH MR', Array('MR', 'MRS'));
        $this->assertTrue(is_array($result));

        $expectedMatches = Array('MR SMITH');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testSuffixes()
    {
        $result = $this->object->removeSuffixes('MR SMITH AC', Array('AC', 'ACDC'));
        $this->assertTrue(is_array($result));

        $expectedMatches = Array('MR SMITH');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testPrefixes_Multiple()
    {
        $result = $this->object->removePrefixes('REV DR JOHN SMITH', Array('REV', 'DR'));
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('JOHN SMITH');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testSuffixes_Multiple()
    {
        $result = $this->object->removeSuffixes('MR SMITH OBE MBCS', Array('MBCS', 'OBE'));
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('MR SMITH');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }


}