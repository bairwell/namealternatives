<?php
/**
 * Tests the Remove Common Words functionality
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests;

/**
 * Tests the remove common words
 * @throws \Exception
 */
class RemoveCommonWordsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \Bairwell\NameAlternatives\Utilities\RemoveCommonWords $object
     */
    protected $object;

    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\RemoveCommonWords');
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function tearDown()
    {
        \Bairwell\DI::reset();
    }

    public function testExceptionString()
    {
        $emess = NULL;
        try {
            $results = $this->object->remove(123, Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Name must be a string', $emess);
    }

    public function testExceptionWords()
    {
        $emess = NULL;
        try {
            $results = $this->object->remove('test', 'hello');
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Words must be an array', $emess);
    }

    public function testExceptionFirst()
    {
        $emess = NULL;
        try {
            $results = $this->object->remove('test', Array(), Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('First separator must be a string or null', $emess);
    }

    public function testExceptionSecond()
    {
        $emess = NULL;
        try {
            $results = $this->object->remove('test', Array(), 'test', Array());
        } catch (\Exception $e) {
            $emess = $e->getMessage();
        }
        $this->assertEquals('Second separator must be a string or null', $emess);
    }

    public function testDefaultSeparators()
    {
        $result = $this->object->remove('THE CAT AND THE DOG SAT ON A MAT', Array('THE', 'AN', 'AND', 'MAT'));
        $this->assertTrue(is_array($result));

        $expectedMatches = Array('THE CAT THE DOG SAT ON A MAT',
            'CAT AND DOG SAT ON A MAT',
            'CAT DOG SAT ON A MAT',
            'THE CAT AND THE DOG SAT ON A',
            'THE CAT THE DOG SAT ON A',
            'CAT AND DOG SAT ON A',
            'CAT DOG SAT ON A');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testCustomSeparatorFirst()
    {
        $result = $this->object->remove('THE CAT XAND THE DOG SAT ON A XMAT', Array('THE', 'AN', 'AND', 'MAT'), 'X');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('THE CAT THE DOG SAT ON A XMAT',
            'THE CAT XAND THE DOG SAT ON A',
            'THE CAT THE DOG SAT ON A');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

    public function testCustomSeparatorSecond()
    {
        $result = $this->object->remove('THEX CAT AND THE DOG SAT ON ANX MAT', Array('THE', 'AN', 'AND', 'MAT'), NULL, 'X');
        $this->assertTrue(is_array($result));
        $expectedMatches = Array('CAT AND THE DOG SAT ON ANX MAT',
            'THEX CAT AND THE DOG SAT ON MAT',
            'CAT AND THE DOG SAT ON MAT');
        $this->assertEquals(count($expectedMatches), count($result));
        $this->assertEquals(0, count(array_diff($expectedMatches, $result)));
    }

}