<?php
/**
 * Tests the Place names code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\PlaceNames;

/**
 * Test class for place names
 */
class CommonEndingsTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * @var \Bairwell\NameAlternatives\GB\PlaceNames\CommonEndings $object
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames\CommonEndings');
        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        $mock = $this->getMock('\Bairwell\NameAlternatives\Utilities\RemoveCommonWords');
        $mock->expects($this->any(), $this->any(), '()')
            ->method('remove')
            ->will($this->returnValue(Array('JOHN SMITH')));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\RemoveCommonWords', $mock);
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $common = Array('BOROUGH', 'SHIRE', 'TON', 'S');
        $expectedChanges = Array();
        foreach ($common as $item) {
            $expectedChanges['JOHN SMITH' . $item] = 'JOHN SMITH';
            $this->names[] = 'JOHN SMITH' . $item;
        }
        $this->runNamesTest($expectedChanges, 'parse');
    }
}