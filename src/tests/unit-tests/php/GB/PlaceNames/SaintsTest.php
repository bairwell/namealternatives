<?php
/**
 * Tests the Place names code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\PlaceNames;

/**
 * Test class for place names
 */
class SaintsTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * @var \Bairwell\NameAlternatives\GB\PlaceNames\Saints $object
     */
    protected $object;

    protected $names = array('ST PATRICK', 'SAINT PAUL', 'ST. MATTHEW', 'ST SWITHINS', 'TORRINGTON STREET', 'LEATHWELL ST');

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames\Saints');
        /**
         * Setup a callback mock for the forenames
         */
        $forenamesMock = $this->getMock('\Bairwell\NameAlternatives\GB\Forenames');
        $callBack = function($value)
        {
            $return = Array();
            switch ($value) {
                case 'PADDY':
                    $return[] = 'PATRICK';
                    break;
                case 'PATRICK':
                    $return[] = 'PADDY';
                    break;
            }
            return $return;
        };

        $forenamesMock->expects($this->any())
            ->method('parse')
            ->with($this->anything())
            ->will($this->returnCallback($callBack));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\GB\Forenames', $forenamesMock);
        /**
         * Setup a mock for the plurals system
         */
        $pluralsMock = $this->getMock('\Bairwell\NameAlternatives\GB\Utilities\PluralsAndPossessions');
        $callBack = function($name)
        {
            $none = preg_replace('/(\'S|S\'|S)( |$)/', '\2', $name);
            if ($none === $name) {
                return Array();
            }
            return Array($none);
        };
        $pluralsMock->expects($this->any())
            ->method('parse')
            ->with($this->anything())
            ->will($this->returnCallback($callBack));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\GB\Utilities\PluralsAndPossessions', $pluralsMock);
        /**
         * Mocks only from this point
         */
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $saintPrefixes = Array('ST', 'SAINT', 'ST.');
        $plurals = Array('S', '\'S', 'S\'');
        $patrick = Array();
        $paul = Array();
        $matthew = Array();
        $swithins = Array();
        foreach ($saintPrefixes as $prefix) {
            if ($prefix !== 'ST') {
                $patrick[] = $prefix . ' PATRICK';
            }
            $patrick[] = $prefix . ' PADDY';
            if ($prefix !== 'SAINT') {
                $paul[] = $prefix . ' PAUL';
            }
            if ($prefix !== 'ST.') {
                $matthew[] = $prefix . ' MATTHEW';
            }
            $swithins[] = $prefix . ' SWITHIN';
            foreach ($plurals as $suffix) {
                $patrick[] = $prefix . ' PATRICK' . $suffix;
                $patrick[] = $prefix . ' PADDY' . $suffix;
                $paul[] = $prefix . ' PAUL' . $suffix;
                $matthew[] = $prefix . ' MATTHEW' . $suffix;
                if (($prefix === 'ST' && $suffix === 'S') === FALSE) {
                    $swithins[] = $prefix . ' SWITHIN' . $suffix;
                }
                $swithins[] = $prefix . ' SWITHINS' . $suffix;
            }
        }
        $expectedChanges = Array(
            'ST PATRICK' => $patrick,
            'SAINT PAUL' => $paul,
            'ST. MATTHEW' => $matthew,
            'ST SWITHINS' => $swithins
        );
        $this->runNamesTest($expectedChanges, 'parse');
    }
}