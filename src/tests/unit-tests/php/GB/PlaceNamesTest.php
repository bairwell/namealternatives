<?php
/**
 * Tests the Place Names code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB;

/**
 * Test class for place names
 */
class PlaceNamesTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * The list of names we are looking to compare against
     * @var array
     */
    protected $names = Array(
        'abc'
    );

    public function testParse_Unittest()
    {
        $this->runOverviewTest('\Bairwell\NameAlternatives\GB\PlaceNames');
    }

    public function testParse_Functional_Abbreviations_Common_RemoveCompassBearings()
    {
        /**
         * @var \Bairwell\NameAlternatives\GB\PlaceNames $object
         */
        $object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames');
        $results = $object->parse('NORTH GOWER ST. PARISH');
        $expectedChanges = Array(
            "NORTH GOWER ST PARISH",
            "NORTH GOWER STREET PARISH",
            "NORTH GOWER ST",
            "NORTH GOWER STREET",
            "GOWER ST PARISH",
            "GOWER STREET PARISH",
            "GOWER ST",
            "GOWER STREET",
            "NORTH GOWER ST PARISHS",
            "NORTH GOWER ST PARISH'S",
            "NORTH GOWER ST PARISHS'",
            "NORTH GOWER SAINT PARISH",
            "NORTH GOWER SAINT PARISHS",
            "NORTH GOWER SAINT PARISH'S",
            "NORTH GOWER SAINT PARISHS'",
            "NORTH GOWER ST. PARISH",
            "NORTH GOWER ST. PARISHS",
            "NORTH GOWER ST. PARISH'S",
            "NORTH GOWER ST. PARISHS'",
            "GOWER ST PARISHS",
            "GOWER ST PARISH'S",
            "GOWER ST PARISHS'",
            "GOWER SAINT PARISH",
            "GOWER SAINT PARISHS",
            "GOWER SAINT PARISH'S",
            "GOWER SAINT PARISHS'",
            "GOWER ST. PARISH",
            "GOWER ST. PARISHS",
            "GOWER ST. PARISH'S",
            "GOWER ST. PARISHS'"
        );
        $this->assertTrue(is_array($results));
        $this->assertEquals(count($results), count($expectedChanges));
        $this->assertEmpty(array_diff($results, $expectedChanges));
    }

    public function testParse_Functional_Saints()
    {
        /**
         * @var \Bairwell\NameAlternatives\GB\PlaceNames $object
         */
        $object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames');
        $results = $object->parse('ST PATRICKS\'S CHURCH');
        $expectedChanges = Array(
            "ST PATRICKSS CHURCH",
            "STREET PATRICKSS CHURCH",
            "ST PATRICKS CHURCH",
            "STREET PATRICKS CHURCH",
            "ST PATRICKSSS CHURCH",
            "ST PATRICKSS'S CHURCH",
            "ST PATRICKSSS' CHURCH",
            "ST PATRIXSS CHURCH",
            "ST PATRIXSSS CHURCH",
            "ST PATRIXSS'S CHURCH",
            "ST PATRIXSSS' CHURCH",
            "ST PATRICKS'S CHURCH",
            "ST PATRICKSS' CHURCH",
            "ST PATRIXS CHURCH",
            "ST PATRIXS'S CHURCH",
            "ST PATRIXSS' CHURCH",
            "ST PATRICK CHURCH",
            "ST PATRICK'S CHURCH",
            "ST PATRICKS' CHURCH",
            "ST PATRIX CHURCH",
            "ST PATRIX'S CHURCH",
            "ST PATRIXS' CHURCH",
            "SAINT PATRICKSS CHURCH",
            "SAINT PATRICKSSS CHURCH",
            "SAINT PATRICKSS'S CHURCH",
            "SAINT PATRICKSSS' CHURCH",
            "SAINT PATRIXSS CHURCH",
            "SAINT PATRIXSSS CHURCH",
            "SAINT PATRIXSS'S CHURCH",
            "SAINT PATRIXSSS' CHURCH",
            "SAINT PATRICKS CHURCH",
            "SAINT PATRICKS'S CHURCH",
            "SAINT PATRICKSS' CHURCH",
            "SAINT PATRIXS CHURCH",
            "SAINT PATRIXS'S CHURCH",
            "SAINT PATRIXSS' CHURCH",
            "SAINT PATRICK CHURCH",
            "SAINT PATRICK'S CHURCH",
            "SAINT PATRICKS' CHURCH",
            "SAINT PATRIX CHURCH",
            "SAINT PATRIX'S CHURCH",
            "SAINT PATRIXS' CHURCH",
            "ST. PATRICKSS CHURCH",
            "ST. PATRICKSSS CHURCH",
            "ST. PATRICKSS'S CHURCH",
            "ST. PATRICKSSS' CHURCH",
            "ST. PATRIXSS CHURCH",
            "ST. PATRIXSSS CHURCH",
            "ST. PATRIXSS'S CHURCH",
            "ST. PATRIXSSS' CHURCH",
            "ST. PATRICKS CHURCH",
            "ST. PATRICKS'S CHURCH",
            "ST. PATRICKSS' CHURCH",
            "ST. PATRIXS CHURCH",
            "ST. PATRIXS'S CHURCH",
            "ST. PATRIXSS' CHURCH",
            "ST. PATRICK CHURCH",
            "ST. PATRICK'S CHURCH",
            "ST. PATRICKS' CHURCH",
            "ST. PATRIX CHURCH",
            "ST. PATRIX'S CHURCH",
            "ST. PATRIXS' CHURCH",
            "ST PATRICIA CHURCH",
            "ST PATRICIAS CHURCH",
            "ST PATRICIA'S CHURCH",
            "ST PATRICIAS' CHURCH",
            "ST PAT CHURCH",
            "ST PATS CHURCH",
            "ST PAT'S CHURCH",
            "ST PATS' CHURCH",
            "ST PATSY CHURCH",
            "ST PATSYS CHURCH",
            "ST PATSY'S CHURCH",
            "ST PATSYS' CHURCH",
            "ST PATTY CHURCH",
            "ST PATTYS CHURCH",
            "ST PATTY'S CHURCH",
            "ST PATTYS' CHURCH",
            "ST TRICIA CHURCH",
            "ST TRICIAS CHURCH",
            "ST TRICIA'S CHURCH",
            "ST TRICIAS' CHURCH",
            "ST TRISH CHURCH",
            "ST TRISHS CHURCH",
            "ST TRISH'S CHURCH",
            "ST TRISHS' CHURCH",
            "ST TRIXIE CHURCH",
            "ST TRIXIES CHURCH",
            "ST TRIXIE'S CHURCH",
            "ST TRIXIES' CHURCH",
            "ST PADDY CHURCH",
            "ST PADDYS CHURCH",
            "ST PADDY'S CHURCH",
            "ST PADDYS' CHURCH",
            "ST PETER CHURCH",
            "ST PETERS CHURCH",
            "ST PETER'S CHURCH",
            "ST PETERS' CHURCH",
            "ST PATSIE CHURCH",
            "ST PATSIES CHURCH",
            "ST PATSIE'S CHURCH",
            "ST PATSIES' CHURCH",
            "ST PATSE CHURCH",
            "ST PATSES CHURCH",
            "ST PATSE'S CHURCH",
            "ST PATSES' CHURCH",
            "ST PATTIE CHURCH",
            "ST PATTIES CHURCH",
            "ST PATTIE'S CHURCH",
            "ST PATTIES' CHURCH",
            "ST PATTE CHURCH",
            "ST PATTES CHURCH",
            "ST PATTE'S CHURCH",
            "ST PATTES' CHURCH",
            "ST TRIXE CHURCH",
            "ST TRIXES CHURCH",
            "ST TRIXE'S CHURCH",
            "ST TRIXES' CHURCH",
            "ST TRIXY CHURCH",
            "ST TRIXYS CHURCH",
            "ST TRIXY'S CHURCH",
            "ST TRIXYS' CHURCH",
            "ST PADDIE CHURCH",
            "ST PADDIES CHURCH",
            "ST PADDIE'S CHURCH",
            "ST PADDIES' CHURCH",
            "ST PADDE CHURCH",
            "ST PADDES CHURCH",
            "ST PADDE'S CHURCH",
            "ST PADDES' CHURCH",
            "ST PATRICIUS CHURCH",
            "ST PATRICIUSS CHURCH",
            "ST PATRICIUS'S CHURCH",
            "ST PATRICIUSS' CHURCH",
            "ST MARTHA CHURCH",
            "ST MARTHAS CHURCH",
            "ST MARTHA'S CHURCH",
            "ST MARTHAS' CHURCH",
            "ST MAT CHURCH",
            "ST MATS CHURCH",
            "ST MAT'S CHURCH",
            "ST MATS' CHURCH",
            "ST MATTY CHURCH",
            "ST MATTYS CHURCH",
            "ST MATTY'S CHURCH",
            "ST MATTYS' CHURCH",
            "ST MATILDA CHURCH",
            "ST MATILDAS CHURCH",
            "ST MATILDA'S CHURCH",
            "ST MATILDAS' CHURCH",
            "ST MATHILDA CHURCH",
            "ST MATHILDAS CHURCH",
            "ST MATHILDA'S CHURCH",
            "ST MATHILDAS' CHURCH",
            "ST MATTIE CHURCH",
            "ST MATTIES CHURCH",
            "ST MATTIE'S CHURCH",
            "ST MATTIES' CHURCH",
            "ST MAUD CHURCH",
            "ST MAUDS CHURCH",
            "ST MAUD'S CHURCH",
            "ST MAUDS' CHURCH",
            "ST TILDA CHURCH",
            "ST TILDAS CHURCH",
            "ST TILDA'S CHURCH",
            "ST TILDAS' CHURCH",
            "ST TILLIE CHURCH",
            "ST TILLIES CHURCH",
            "ST TILLIE'S CHURCH",
            "ST TILLIES' CHURCH",
            "ST PETE CHURCH",
            "ST PETES CHURCH",
            "ST PETE'S CHURCH",
            "ST PETES' CHURCH",
            "ST PETERKIN CHURCH",
            "ST PETERKINS CHURCH",
            "ST PETERKIN'S CHURCH",
            "ST PETERKINS' CHURCH",
            "ST PET CHURCH",
            "ST PETS CHURCH",
            "ST PET'S CHURCH",
            "ST PETS' CHURCH",
            "ST PADRUIG CHURCH",
            "ST PADRUIGS CHURCH",
            "ST PADRUIG'S CHURCH",
            "ST PADRUIGS' CHURCH",
            "ST TRICKIE CHURCH",
            "ST TRICKIES CHURCH",
            "ST TRICKIE'S CHURCH",
            "ST TRICKIES' CHURCH",
            "ST TRICKE CHURCH",
            "ST TRICKES CHURCH",
            "ST TRICKE'S CHURCH",
            "ST TRICKES' CHURCH",
            "ST TRICKY CHURCH",
            "ST TRICKYS CHURCH",
            "ST TRICKY'S CHURCH",
            "ST TRICKYS' CHURCH",
            "ST PATRICIU CHURCH",
            "ST PATRICIU'S CHURCH",
            "ST PATRICIUS' CHURCH",
            "SAINT PATRICIA CHURCH",
            "SAINT PATRICIAS CHURCH",
            "SAINT PATRICIA'S CHURCH",
            "SAINT PATRICIAS' CHURCH",
            "SAINT PAT CHURCH",
            "SAINT PATS CHURCH",
            "SAINT PAT'S CHURCH",
            "SAINT PATS' CHURCH",
            "SAINT PATSY CHURCH",
            "SAINT PATSYS CHURCH",
            "SAINT PATSY'S CHURCH",
            "SAINT PATSYS' CHURCH",
            "SAINT PATTY CHURCH",
            "SAINT PATTYS CHURCH",
            "SAINT PATTY'S CHURCH",
            "SAINT PATTYS' CHURCH",
            "SAINT TRICIA CHURCH",
            "SAINT TRICIAS CHURCH",
            "SAINT TRICIA'S CHURCH",
            "SAINT TRICIAS' CHURCH",
            "SAINT TRISH CHURCH",
            "SAINT TRISHS CHURCH",
            "SAINT TRISH'S CHURCH",
            "SAINT TRISHS' CHURCH",
            "SAINT TRIXIE CHURCH",
            "SAINT TRIXIES CHURCH",
            "SAINT TRIXIE'S CHURCH",
            "SAINT TRIXIES' CHURCH",
            "SAINT PADDY CHURCH",
            "SAINT PADDYS CHURCH",
            "SAINT PADDY'S CHURCH",
            "SAINT PADDYS' CHURCH",
            "SAINT PETER CHURCH",
            "SAINT PETERS CHURCH",
            "SAINT PETER'S CHURCH",
            "SAINT PETERS' CHURCH",
            "SAINT PATSIE CHURCH",
            "SAINT PATSIES CHURCH",
            "SAINT PATSIE'S CHURCH",
            "SAINT PATSIES' CHURCH",
            "SAINT PATSE CHURCH",
            "SAINT PATSES CHURCH",
            "SAINT PATSE'S CHURCH",
            "SAINT PATSES' CHURCH",
            "SAINT PATTIE CHURCH",
            "SAINT PATTIES CHURCH",
            "SAINT PATTIE'S CHURCH",
            "SAINT PATTIES' CHURCH",
            "SAINT PATTE CHURCH",
            "SAINT PATTES CHURCH",
            "SAINT PATTE'S CHURCH",
            "SAINT PATTES' CHURCH",
            "SAINT TRIXE CHURCH",
            "SAINT TRIXES CHURCH",
            "SAINT TRIXE'S CHURCH",
            "SAINT TRIXES' CHURCH",
            "SAINT TRIXY CHURCH",
            "SAINT TRIXYS CHURCH",
            "SAINT TRIXY'S CHURCH",
            "SAINT TRIXYS' CHURCH",
            "SAINT PADDIE CHURCH",
            "SAINT PADDIES CHURCH",
            "SAINT PADDIE'S CHURCH",
            "SAINT PADDIES' CHURCH",
            "SAINT PADDE CHURCH",
            "SAINT PADDES CHURCH",
            "SAINT PADDE'S CHURCH",
            "SAINT PADDES' CHURCH",
            "SAINT PATRICIUS CHURCH",
            "SAINT PATRICIUSS CHURCH",
            "SAINT PATRICIUS'S CHURCH",
            "SAINT PATRICIUSS' CHURCH",
            "SAINT MARTHA CHURCH",
            "SAINT MARTHAS CHURCH",
            "SAINT MARTHA'S CHURCH",
            "SAINT MARTHAS' CHURCH",
            "SAINT MAT CHURCH",
            "SAINT MATS CHURCH",
            "SAINT MAT'S CHURCH",
            "SAINT MATS' CHURCH",
            "SAINT MATTY CHURCH",
            "SAINT MATTYS CHURCH",
            "SAINT MATTY'S CHURCH",
            "SAINT MATTYS' CHURCH",
            "SAINT MATILDA CHURCH",
            "SAINT MATILDAS CHURCH",
            "SAINT MATILDA'S CHURCH",
            "SAINT MATILDAS' CHURCH",
            "SAINT MATHILDA CHURCH",
            "SAINT MATHILDAS CHURCH",
            "SAINT MATHILDA'S CHURCH",
            "SAINT MATHILDAS' CHURCH",
            "SAINT MATTIE CHURCH",
            "SAINT MATTIES CHURCH",
            "SAINT MATTIE'S CHURCH",
            "SAINT MATTIES' CHURCH",
            "SAINT MAUD CHURCH",
            "SAINT MAUDS CHURCH",
            "SAINT MAUD'S CHURCH",
            "SAINT MAUDS' CHURCH",
            "SAINT TILDA CHURCH",
            "SAINT TILDAS CHURCH",
            "SAINT TILDA'S CHURCH",
            "SAINT TILDAS' CHURCH",
            "SAINT TILLIE CHURCH",
            "SAINT TILLIES CHURCH",
            "SAINT TILLIE'S CHURCH",
            "SAINT TILLIES' CHURCH",
            "SAINT PETE CHURCH",
            "SAINT PETES CHURCH",
            "SAINT PETE'S CHURCH",
            "SAINT PETES' CHURCH",
            "SAINT PETERKIN CHURCH",
            "SAINT PETERKINS CHURCH",
            "SAINT PETERKIN'S CHURCH",
            "SAINT PETERKINS' CHURCH",
            "SAINT PET CHURCH",
            "SAINT PETS CHURCH",
            "SAINT PET'S CHURCH",
            "SAINT PETS' CHURCH",
            "SAINT PADRUIG CHURCH",
            "SAINT PADRUIGS CHURCH",
            "SAINT PADRUIG'S CHURCH",
            "SAINT PADRUIGS' CHURCH",
            "SAINT TRICKIE CHURCH",
            "SAINT TRICKIES CHURCH",
            "SAINT TRICKIE'S CHURCH",
            "SAINT TRICKIES' CHURCH",
            "SAINT TRICKE CHURCH",
            "SAINT TRICKES CHURCH",
            "SAINT TRICKE'S CHURCH",
            "SAINT TRICKES' CHURCH",
            "SAINT TRICKY CHURCH",
            "SAINT TRICKYS CHURCH",
            "SAINT TRICKY'S CHURCH",
            "SAINT TRICKYS' CHURCH",
            "SAINT PATRICIU CHURCH",
            "SAINT PATRICIU'S CHURCH",
            "SAINT PATRICIUS' CHURCH",
            "ST. PATRICIA CHURCH",
            "ST. PATRICIAS CHURCH",
            "ST. PATRICIA'S CHURCH",
            "ST. PATRICIAS' CHURCH",
            "ST. PAT CHURCH",
            "ST. PATS CHURCH",
            "ST. PAT'S CHURCH",
            "ST. PATS' CHURCH",
            "ST. PATSY CHURCH",
            "ST. PATSYS CHURCH",
            "ST. PATSY'S CHURCH",
            "ST. PATSYS' CHURCH",
            "ST. PATTY CHURCH",
            "ST. PATTYS CHURCH",
            "ST. PATTY'S CHURCH",
            "ST. PATTYS' CHURCH",
            "ST. TRICIA CHURCH",
            "ST. TRICIAS CHURCH",
            "ST. TRICIA'S CHURCH",
            "ST. TRICIAS' CHURCH",
            "ST. TRISH CHURCH",
            "ST. TRISHS CHURCH",
            "ST. TRISH'S CHURCH",
            "ST. TRISHS' CHURCH",
            "ST. TRIXIE CHURCH",
            "ST. TRIXIES CHURCH",
            "ST. TRIXIE'S CHURCH",
            "ST. TRIXIES' CHURCH",
            "ST. PADDY CHURCH",
            "ST. PADDYS CHURCH",
            "ST. PADDY'S CHURCH",
            "ST. PADDYS' CHURCH",
            "ST. PETER CHURCH",
            "ST. PETERS CHURCH",
            "ST. PETER'S CHURCH",
            "ST. PETERS' CHURCH",
            "ST. PATSIE CHURCH",
            "ST. PATSIES CHURCH",
            "ST. PATSIE'S CHURCH",
            "ST. PATSIES' CHURCH",
            "ST. PATSE CHURCH",
            "ST. PATSES CHURCH",
            "ST. PATSE'S CHURCH",
            "ST. PATSES' CHURCH",
            "ST. PATTIE CHURCH",
            "ST. PATTIES CHURCH",
            "ST. PATTIE'S CHURCH",
            "ST. PATTIES' CHURCH",
            "ST. PATTE CHURCH",
            "ST. PATTES CHURCH",
            "ST. PATTE'S CHURCH",
            "ST. PATTES' CHURCH",
            "ST. TRIXE CHURCH",
            "ST. TRIXES CHURCH",
            "ST. TRIXE'S CHURCH",
            "ST. TRIXES' CHURCH",
            "ST. TRIXY CHURCH",
            "ST. TRIXYS CHURCH",
            "ST. TRIXY'S CHURCH",
            "ST. TRIXYS' CHURCH",
            "ST. PADDIE CHURCH",
            "ST. PADDIES CHURCH",
            "ST. PADDIE'S CHURCH",
            "ST. PADDIES' CHURCH",
            "ST. PADDE CHURCH",
            "ST. PADDES CHURCH",
            "ST. PADDE'S CHURCH",
            "ST. PADDES' CHURCH",
            "ST. PATRICIUS CHURCH",
            "ST. PATRICIUSS CHURCH",
            "ST. PATRICIUS'S CHURCH",
            "ST. PATRICIUSS' CHURCH",
            "ST. MARTHA CHURCH",
            "ST. MARTHAS CHURCH",
            "ST. MARTHA'S CHURCH",
            "ST. MARTHAS' CHURCH",
            "ST. MAT CHURCH",
            "ST. MATS CHURCH",
            "ST. MAT'S CHURCH",
            "ST. MATS' CHURCH",
            "ST. MATTY CHURCH",
            "ST. MATTYS CHURCH",
            "ST. MATTY'S CHURCH",
            "ST. MATTYS' CHURCH",
            "ST. MATILDA CHURCH",
            "ST. MATILDAS CHURCH",
            "ST. MATILDA'S CHURCH",
            "ST. MATILDAS' CHURCH",
            "ST. MATHILDA CHURCH",
            "ST. MATHILDAS CHURCH",
            "ST. MATHILDA'S CHURCH",
            "ST. MATHILDAS' CHURCH",
            "ST. MATTIE CHURCH",
            "ST. MATTIES CHURCH",
            "ST. MATTIE'S CHURCH",
            "ST. MATTIES' CHURCH",
            "ST. MAUD CHURCH",
            "ST. MAUDS CHURCH",
            "ST. MAUD'S CHURCH",
            "ST. MAUDS' CHURCH",
            "ST. TILDA CHURCH",
            "ST. TILDAS CHURCH",
            "ST. TILDA'S CHURCH",
            "ST. TILDAS' CHURCH",
            "ST. TILLIE CHURCH",
            "ST. TILLIES CHURCH",
            "ST. TILLIE'S CHURCH",
            "ST. TILLIES' CHURCH",
            "ST. PETE CHURCH",
            "ST. PETES CHURCH",
            "ST. PETE'S CHURCH",
            "ST. PETES' CHURCH",
            "ST. PETERKIN CHURCH",
            "ST. PETERKINS CHURCH",
            "ST. PETERKIN'S CHURCH",
            "ST. PETERKINS' CHURCH",
            "ST. PET CHURCH",
            "ST. PETS CHURCH",
            "ST. PET'S CHURCH",
            "ST. PETS' CHURCH",
            "ST. PADRUIG CHURCH",
            "ST. PADRUIGS CHURCH",
            "ST. PADRUIG'S CHURCH",
            "ST. PADRUIGS' CHURCH",
            "ST. TRICKIE CHURCH",
            "ST. TRICKIES CHURCH",
            "ST. TRICKIE'S CHURCH",
            "ST. TRICKIES' CHURCH",
            "ST. TRICKE CHURCH",
            "ST. TRICKES CHURCH",
            "ST. TRICKE'S CHURCH",
            "ST. TRICKES' CHURCH",
            "ST. TRICKY CHURCH",
            "ST. TRICKYS CHURCH",
            "ST. TRICKY'S CHURCH",
            "ST. TRICKYS' CHURCH",
            "ST. PATRICIU CHURCH",
            "ST. PATRICIU'S CHURCH",
            "ST. PATRICIUS' CHURCH",
            "STREET PATRIXSS CHURCH",
            "STREET PATRIXS CHURCH",
            "STREET PATRICK CHURCH",
            "STREET PATRIX CHURCH"
        );
        $this->assertTrue(is_array($results));
        $this->assertEquals(count($results), count($expectedChanges));
        $this->assertEmpty(array_diff($results, $expectedChanges));
    }

    public function testParse_Functional_Cotes_CommonEndings()
    {
        /**
         * @var \Bairwell\NameAlternatives\GB\PlaceNames $object
         */
        $object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames');

        $results = $object->parse('HUNCOTE IN LEICESTERSHIRE');
        $expectedChanges = Array(
            "HUNCOTE IN LEICESTERSHIRE",
            "HUNCOTE IN LEICESTER",
            "HUNCITE IN LEICESTERSHIRE",
            "HUNCOAT IN LEICESTERSHIRE",
            "HUNCITE IN LEICESTER",
            "HUNCOAT IN LEICESTER",
            "HUNCOTE IN LESTER",
            "HUNCOTE IN LEICS",
            "HUNCITE IN LESTER",
            "HUNCITE IN LEICS",
            "HUNCOAT IN LESTER",
            "HUNCOAT IN LEICS",
            "HUNCOTE IN LEIC",
            "HUNCITE IN LEIC",
            "HUNCOAT IN LEIC"
        );
        $this->assertTrue(is_array($results));
        $this->assertEquals(count($results), count($expectedChanges));
        $this->assertEmpty(array_diff($results, $expectedChanges));
    }

    public function testParse_Functional_RChiswell_Wikipedia()
    {
        /**
         * @var \Bairwell\NameAlternatives\GB\PlaceNames $object
         */
        $object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PlaceNames');
        $results = $object->parse('WOOTTON BASSETT NEAR CASNEWYDD');
        $expectedChanges = Array(
            "WOOTTON BASSETT NEAR CASNEWYDD",
            "WOOT BASSETT NEAR CASNEWYDD",
            "ROYAL WOOTTON BASSETT NEAR CASNEWYDD",
            "WOOTTON BASSETT NEAR NEWPORT-ON-USK",
            "WOOTTON BASSETT NEAR CASNEWYDD-AR-WYSG",
            "WOOT BASSETT NEAR NEWPORT-ON-USK",
            "WOOT BASSETT NEAR CASNEWYDD-AR-WYSG",
            "ROYAL WOOTTON BASSETT NEAR NEWPORT-ON-USK",
            "ROYAL WOOTTON BASSETT NEAR CASNEWYDD-AR-WYSG"
        );
        $this->assertTrue(is_array($results));
        $this->assertEquals(count($results), count($expectedChanges));
        $this->assertEmpty(array_diff($results, $expectedChanges));
    }
}