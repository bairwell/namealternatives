<?php
/**
 * Tests the GB Surnames code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\Surnames;

/**
 * Test class for Wingops
 */
class WingOpsTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * The list of names we are looking to compare against
     * @var array
     */
    protected $names = Array(
        'BARBARA', 'ANDREA'
    );

    /**
     * @var \Bairwell\NameAlternatives\GB\Surnames\Wingops $object
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\Surnames\Wingops');
        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        /**
         * Only two names are included in this mock
         */
        $expectedChanges = Array(
            'BARBARA' => Array('BABS', 'BARB', 'BARBIE', 'BARBY', 'BOBBIE', 'BONNIE', 'BARBERY'),
            'ANDREA' => Array('ANDREW', 'DREA', 'REA')
        );
        $filesToNames = Array(
            'GB' . DIRECTORY_SEPARATOR . 'Surnames' . DIRECTORY_SEPARATOR . 'data'
                . DIRECTORY_SEPARATOR . 'wingops.csv'
            => Array(
                Array('BARBARA', 'BABS', 'BARB', 'BARBIE', 'BARBY', 'BOBBIE', 'BONNIE', 'BARBERY'),
                Array('ANDREA', 'ANDREW', 'DREA', 'REA')
            )
        );
        $this->setupMockCommentedCSV($filesToNames);
        $this->runNamesTest($expectedChanges, 'parse');
    }
}