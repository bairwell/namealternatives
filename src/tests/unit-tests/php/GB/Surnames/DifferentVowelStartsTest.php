<?php
/**
 * Tests the GB Surnames code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\Surnames;

/**
 * Test class for Different Vowel Starts
 */
class DifferentVowelStartsTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * The list of names we are looking to compare against
     * @var array
     */
    protected $names = Array(
        'ALAN', 'IAIN', 'FRANK', 'ELANE', 'OTANG', 'URSULA'
    );

    /**
     * @var \Bairwell\NameAlternatives\GB\Surnames\Wingops $object
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\Surnames\DifferentVowelStarts');
        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $expectedChanges = Array(
            'ALAN' => Array('ELAN', 'ILAN', 'OLAN', 'ULAN'),
            'IAIN' => Array('AAIN', 'EAIN', 'OAIN', 'UAIN'),
            'ELANE' => Array('ALANE', 'ILANE', 'OLANE', 'ULANE'),
            'OTANG' => Array('ATANG', 'ETANG', 'ITANG', 'UTANG'),
            'URSULA' => Array('ARSULA', 'ERSULA', 'IRSULA', 'ORSULA')
        );
        $this->runNamesTest($expectedChanges, 'parse');
    }
}