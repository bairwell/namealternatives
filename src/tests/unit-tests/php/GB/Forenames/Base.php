<?php
/**
 * Tests individual GB Forenames submodules code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\Forenames;

/**
 * Test class for Forenames submodules
 */
abstract class Base extends \PHPUnit_Framework_TestCase
{
    /**
     * The list of names we are looking to compare against
     * @var array
     */
    protected $names = Array(
        'RICHARD', 'KATY', 'ROBERT', 'HUMPLEDINK', 'SAM', 'PETE', 'BARBARA', 'LEMBIT', 'SIÂN', 'GAIL', 'ZACHARIAH',
        'LAURIE', 'THÉRÈSE', 'LINDSEY', 'JERRY', 'MARC', 'DAVID', 'NICK', 'STEF', 'STEPH', 'ALDYTH', 'AMELIA',
        'ANDREA', 'JO', 'BEKI', 'CHRIS', 'CORALIE', 'COS', 'FRANCIS', 'HOLLIE', 'JACQUELINE', 'JEN', 'STEPHANIE',
        'TONI', 'BEX'
    );

    /**
     * @var object $object
     */
    protected $object;

    protected function setupMockCommentedCSV()
    {
        $filesToNames = Array(
            'GB' . DIRECTORY_SEPARATOR . 'Forenames' . DIRECTORY_SEPARATOR . 'carlton.csv' => Array(
                Array('BARBARA', 'BABS', 'BARB', 'BARBIE', 'BARBY', 'BOBBIE', 'BONNIE', 'BARBERY'),
                Array('ANDREA', 'ANDREW', 'DREA', 'REA')
            )
        );
        $commentedCSVMock = $this->getMock(
            '\Bairwell\NameAlternatives\Tests\Fixtures\CommentedStub',
            array('searchForCSVmatches')
        );
        $callBack = function($filename, $name) use ($filesToNames)
        {
            $matchedName = NULL;
            foreach ($filesToNames as $thisfilename => $data) {
                if (mb_substr($filename, -(mb_strlen($thisfilename))) === $thisfilename) {
                    $matchedName = $thisfilename;
                }
            }
            if ($matchedName === NULL) {
                throw new \Exception('Unrecognised file name ' . $filename . ' for name ' . $name);
            }
            $return = Array();
            foreach ($filesToNames[$matchedName] as $namearray) {
                if (in_array($name, $namearray) === TRUE) {
                    foreach ($namearray as $newname) {
                        if ($newname !== $name) {
                            $return[] = $newname;
                        }
                    }
                }
            }
            return $return;
        };
        $commentedCSVMock->expects($this->any())
            ->method('searchForCSVmatches')
            ->with($this->anything(), $this->anything(), $this->anything())
            ->will($this->returnCallback($callBack));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\CommentedCSV', $commentedCSVMock);
    }

    protected function runNamesTest($expectedChanges, $function)
    {
        foreach ($this->names as $name) {
            $results = $this->object->$function($name);
            $this->assertTrue(
                is_array($results), 'When checking ' . $name . ' for ' . $function . ' did not get array in return'
            );
            if (isset($expectedChanges[$name]) === TRUE) {
                if (is_array($expectedChanges[$name]) === TRUE) {
                    $imploded = implode(',', $results);
                    $this->assertEquals(
                        count($expectedChanges[$name]), count($results),
                        'When checking ' . $name . ' for ' . $function . ' got ' . $imploded
                    );
                    $this->assertEmpty(
                        array_diff($results, $expectedChanges[$name]),
                        'When checking ' . $name . ' for ' . $function . ' got ' . $imploded
                    );
                    //$this->assertSame($expectedChanges[$name], $results, 'When checking ' . $name . ' for ' . $function . ' got ' . $imploded);
                } else {
                    if (is_string($expectedChanges[$name]) === TRUE) {
                        $count = count($results);
                        $imploded = '';
                        if ($count > 0) {
                            $imploded = implode('*,*', $results);
                        }
                        $this->assertEquals(
                            1, $count,
                            'When checking ' . $name . ' for string ' . $function . ' expected 1, got : ' . $imploded
                        );
                        $this->assertEquals(
                            $expectedChanges[$name], array_pop($results), 'When checking ' . $name . ' for ' . $function
                        );
                    } else {
                        throw new \Exception('Unrecognised check');
                    }
                }
            } else {
                $count = count($results);
                if ($count > 0) {
                    if ($count === 1) {
                        $this->fail($function . ' changed ' . $name . ' to ' . $results[0]);
                    } else {
                        $imploded = implode(',', $results);
                        $this->fail(
                            $function . ' returned ' . count($results) . ' when processing ' . $name . ' : ' . $imploded
                        );
                    }
                }
            }
        }
    }

    public function testParse()
    {
        /**
         * Only two names are included in this mock
         */
        $expectedChanges = Array(
            'BARBARA' => Array('BABS', 'BARB', 'BARBIE', 'BARBY', 'BOBBIE', 'BONNIE', 'BARBERY'),
            'ANDREA' => Array('ANDREW', 'DREA', 'REA')
        );
        $this->setupMockCommentedCSV();
        $this->runNamesTest($expectedChanges, 'parse');
    }
}