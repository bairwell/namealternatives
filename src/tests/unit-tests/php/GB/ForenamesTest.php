<?php
/**
 * Tests the Forenames code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB;

/**
 * Test class for forenames
 */
class ForenamesTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * The list of names we are looking to compare against
     * @var array
     */
    protected $names = Array(
        'abc'
    );

    public function testParse_Unittest()
    {
        $this->runOverviewTest('\Bairwell\NameAlternatives\GB\Forenames');
    }

    public function testParse_Functional()
    {
        /**
         * @var \Bairwell\NameAlternatives\GB\Forenames $object
         */
        $object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\Forenames');
        $results = $object->parse('RICHARD');
        $expectedChanges = Array(
            "RICHARD",
            "DICK",
            "RICHIE",
            "RICK",
            "RITCHIE",
            "RICHE",
            "RICHY",
            "RITCHE",
            "RITCHY",
            "DICKIE",
            "DICKY",
            "DICKEN",
            "DICKON",
            "RODERICK",
            "RODERIC",
            "ROD",
            "RODDIE",
            "RODDY",
            "RICH",
            "RIK",
            "RICKY",
            "RIC",
            "RICARD",
            "RICARDO",
            "RIOCHARD",
            "RIKHARD",
            "RICKIE",
            "RIKKI",
            "RICHU",
            "DIX",
            "RIX",
            "DIXIE",
            "DIXY",
            "DIXEN",
            "DIXON",
            "RODERIX",
            "RIXY",
            "RIXIE"
        );
        $this->assertTrue(is_array($results));
        $this->assertEquals(count($expectedChanges), count($results));
        $this->assertEmpty(array_diff($results, $expectedChanges));
    }
}