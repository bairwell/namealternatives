<?php
/**
 * Tests the Person Titles code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\PersonTitles;

/**
 * Test class for person titles
 */
class LegislativeTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * @var \Bairwell\NameAlternatives\GB\PersonTitles\Legislative $object
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PersonTitles\Legislative');
        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        $mock = $this->getMock(
            '\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes',
            array('removePrefixes')
        );
        $mock->expects($this->any())
            ->method('removePrefixes')
            ->will($this->returnValue(Array('JOHN SMITH')));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes', $mock);
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $titles = Array(
            'ALDERMAN', 'COUNCILLOR', 'DELEGATE', 'MEMBER OF CONGRESS', 'MEMBER OF PARLIAMENT',
            'MEMBER OF THE EUROPEAN PARLIAMENT', 'MEMBER OF THE SCOTTISH PARLIAMENT', 'MEMBER OF PROVINCIAL PARLIAMENT',
            'MEMBER OF THE NATIONAL ASSEMBLY', 'MEMBER OF THE LEGISLATIVE COUNCIL',
            'MEMBER OF THE LEGISLATIVE ASSEMBLY', 'MLA', 'MEMBER OF THE HOUSE OF REPRESENTATIVES',
            'MEMBER OF THE HOUSE OF ASSEMBLY', 'REPRESENTATIVE', 'SENATOR', 'SPEAKER', 'PRIME MINISTER',
            'DEPUTY PRIME MINISTER'
        );
        $expectedChanges = Array();
        foreach ($titles as $title) {
            $expectedChanges[$title . ' JOHN SMITH'] = 'JOHN SMITH';
            $this->names[] = $title . ' JOHN SMITH';
        }
        $this->runNamesTest($expectedChanges, 'parse');
    }
}