<?php
/**
 * Tests the Person Titles code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\PersonTitles;

/**
 * Test class for person titles
 */
class PostNominalsTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * @var \Bairwell\NameAlternatives\GB\PersonTitles\PostNominals $object
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\PersonTitles\PostNominals');
        /**
         * Any other factory calls need to be to our closures/mocks. Let's make it so.
         */
        $mock = $this->getMock(
            '\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes',
            array('removeSuffixes')
        );
        $mock->expects($this->any())
            ->method('removeSuffixes')
            ->will($this->returnValue(Array('JOHN SMITH')));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes', $mock);
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $titles = Array(
            // Orders - http://en.wikipedia.org/wiki/List_of_post-nominal_letters_%28United_Kingdom%29
            'VC', 'GC', 'KG', 'LG', 'KT', 'LT', 'KP', 'GCB', 'OM', 'GCSI', 'GCMG', 'GCIE', 'GCVO', 'GBE', 'CH', 'KCB',
            'DCB', 'KCSI', 'KCMG', 'DCMG', 'KCIE', 'KCVO', 'DCVO', 'KBE', 'DBE', 'CB', 'CSI', 'CMG', 'CIE', 'CVO',
            'CBE', 'DSO', 'LVO', 'OBE', 'ISO', 'MVO', 'MBE', 'IOM', 'CGC', 'RRC', 'DSC', 'MC', 'DFC', 'AFC', 'ARRC',
            'OBI', 'DCM', 'CGM', 'GM', 'IDSM', 'DSM', 'MM', 'DFM', 'AFM', 'SGM', 'IOM', 'CPM', 'QGM', 'BEM', 'QPM',
            'QFSM', 'CPM', 'MSM', 'ERD', 'VD', 'TD', 'UD', 'ED', 'RD', 'VRD', 'AE', 'GCSTJ', 'KSTJ', 'DSTJ', 'CHSTJ',
            'CSTJ', 'OSTJ', 'SBSTJ', 'SSSTJ', 'MSTJ', 'ESQSTJ',
            // Appointments - http://en.wikipedia.org/wiki/List_of_post-nominal_letters_%28United_Kingdom%29
            'PC', 'ADC', 'QHP', 'QHS', 'QHDS', 'QHNS', 'QHC', 'SCJ', 'J', 'LJ', 'QS', 'SL', 'QC', 'KC', 'JP', 'DL',
            'MP', 'MSP', 'AM', 'AM', 'MLA', 'MEP', 'PM',
            // Professional - http://en.wikipedia.org/wiki/List_of_post-nominal_letters_%28United_Kingdom%29
            'BHAM', 'BRIS', 'CANTAB', 'CEST', 'CICEST', 'DE MONT', 'DUND', 'DUNELM', 'EBOR', 'EDIN', 'EXON', 'GLAM',
            'GLAS', 'LSE', 'LEIC', 'LOND', 'MANC', 'N\'CLE', 'NOTT', 'OPEN', 'OXON', 'PORTS', 'SOTON', 'ST AND',
            'SURREY', 'UEA', 'WALES', 'WIGORN', 'WINTON', 'A.INST.L.EX', 'ACSM/MCSM', 'AKC', 'ARCS', 'ARSM', 'AUS',
            'BA', 'BARCH', 'BCL', 'BCH/BS/BCH/BCHIR', 'BED', 'BENG', 'BN', 'BSC', 'BSC PHYS', 'BVM&S', 'BVSC',
            'BVETMED', 'CENG', 'CMILT', 'CML', 'CMARENG', 'CMARSCI', 'CMARTECH', 'CPL', 'CPHT', 'CTL', 'CTP', 'CERT',
            'CERT. ED.', 'CERTHE', 'CHB/BM', 'DBENV', 'DCL', 'DCONSTMGT', 'DD', 'DIC', 'DIMC RCSED', 'DLP', 'DLITT',
            'DPT', 'DPHIL', 'DPROF', 'DREST', 'DSOCSCI', 'DIP', 'DIP. ARCH', 'DIPHE', 'DIPLP', 'DIPLAW/CPE', 'EJLOG',
            'EMLOG', 'EN', 'ENM', 'ESLOG', 'ENGD', 'ENGTECH', 'F.INST.L.EX', 'FADO', 'FAWM', 'FBDO', 'FCEM', 'FCILT',
            'FCOPTOM', 'FCSP', 'FFHOM', 'FFPMRCA', 'FIMC RCSED', 'FRCA', 'FRCGP', 'FRCOG', 'FRCP', 'FRCPCH', 'FRCPSYCH',
            'FRCS', 'FRCVS', 'FSCR', 'FDA', 'FDENG', 'FDSC', 'G.INST.L.EX', 'HNC/HNCERT', 'HND/HNDIP', 'IENG',
            'IMARENG', 'JRLOG', 'LFHOM', 'LLB', 'LLD', 'LLM', 'LPE', 'LOG', 'MA', 'MACC', 'MARCH', 'MB/BM', 'MBA',
            'MBCHB', 'MCEM', 'MCOPTOM', 'MCSP', 'MCH', 'MCHEM', 'MCOMP', 'MD', 'MED', 'MENG', 'MENT', 'MFHOM', 'MFIN',
            'MGEOL', 'MILT', 'MJUR', 'MLITT', 'MM', 'MMATH', 'MMUS', 'MOST', 'MPA', 'MPHIL', 'MPHYS', 'MRCGP', 'MRCOG',
            'MRCP', 'MRCPCH', 'MRCPATH', 'MRCPSYCH', 'MRCS', 'MRCVS', 'MRES', 'MS', 'MSCR', 'MSC', 'MSCI', 'MSOCSC',
            'MST', 'MARENGTECH', 'MASTER MARINER', 'NPQH', 'PGCE', 'PGD', 'PGDCCI', 'PGDE', 'PLS', 'PLOG', 'PGDIP',
            'PHD', 'QTS', 'RFHN', 'RGN', 'RHV', 'RM', 'RMN', 'RN', 'RN1', 'RN2', 'RN3', 'RN4', 'RN5', 'RN6', 'RN7',
            'RN8', 'RN9', 'RNA', 'RNC', 'RNLD', 'RNMH', 'ROH', 'RSCN', 'RSN', 'RVN', 'REG PHARM TECH', 'SAC CERT',
            'SAC DIP', 'SCHM', 'SCLD', 'SEN', 'SENM', 'SPAN', 'SPCC', 'SPCN', 'SPDN', 'SPHP', 'SPLD', 'SPMH', 'SRN',
            'SRLOG', 'V100', 'V200', 'V300', 'VN', 'VETMB', 'PH.D', 'PG CERT', 'BA (HONS)',
            // Fellowships -  http://en.wikipedia.org/wiki/List_of_post-nominal_letters_%28United_Kingdom%29
            'A.INST.SRM', 'ACGI', 'ACIARB', 'ACIH', 'ACII', 'ACIM', 'ACIPD', 'ASSOC CIPD', 'ACIPR', 'ACIS', 'ACLIP',
            'ACMA', 'ACQI', 'ACSI', 'AFSEDA', 'AIBS', 'AIIM', 'AIITT', 'AINSTLM', 'AMBCS', 'AMBES', 'AMIAP', 'AMICE',
            'AMIHT', 'AMINSTP', 'AMIPLANTE', 'AMIRTE', 'AMISTRUCTE', 'AMRI', 'AMRSC', 'AMSOE', 'ARA', 'ARBS', 'ASPE',
            'ASTA', 'BH', 'BH(D)', 'BH(DS)', 'BH(OS)', 'CBIOL', 'CCHEM', 'CCOL', 'CDIR', 'CENV', 'CFIOSH', 'CGEOG',
            'CHARTERED INSURER', 'CHARTERED MARKETER', 'CHARTERED SECRETARY', 'CIITT', 'CIOJ', 'CITP', 'CMALT', 'CMATH',
            'CMC', 'CMGR', 'CMILT', 'CPA', 'CPHYS', 'CSCI', 'CSTAT', 'CSYP', 'DSPE', 'FAAV', 'FAPM', 'FAWM', 'FBA',
            'FBCS', 'FBCS CITP', 'FBDO', 'FBES', 'FBS', 'FCA', 'FCCA', 'FCCS', 'FCGI', 'FCIARB', 'FCIEH', 'FCIH',
            'FCII', 'FCIL', 'FCILT', 'FCIM', 'FCIOB', 'FCIPD', 'FCIPR', 'FCIPS', 'FCIS', 'FCLIP', 'FCMA', 'FCMI',
            'FCPARA', 'FCQI', 'FCSD', 'FCSI', 'FCSP', 'FEIS', 'FEPS', 'FFA', 'FFB', 'FFLYM', 'FGS', 'FHEA', 'FIA',
            'FIAP', 'FIBM', 'FIBMS', 'FIBS', 'FICE', 'FICHEME', 'FIED', 'FIEE', 'FIEEM', 'FIET', 'FIFL', 'FIHT',
            'FIITT', 'FILSA', 'FIMA', 'FIMECHE', 'FINSTCPD', 'FINSTP', 'FINSTR', 'FINSTRE', 'FIOD', 'FIOS', 'FIPLANTE',
            'FIRTE', 'FISTRUCTE', 'FLS', 'FMAAT', 'FMEDSCI', 'FNCM', 'FNI', 'FRAES', 'FRAI', 'FRAS', 'FRBS', 'FRCA',
            'FRCGP', 'FRCO', 'FRCP', 'FRCS', 'FRENG', 'FRGS', 'FRHISTS', 'FRI', 'FRICS', 'FRIN', 'FRMETS', 'FRNS',
            'FRPHARMS', 'FRS', 'FRSA', 'FRSC', 'FRSE', 'FRSH', 'FRSM', 'FRSTMH', 'FRSTM&H', 'FRUSI', 'FSA',
            'FSA (SCOT)', 'FSB', 'FSEDA', 'FSHAA', 'FSOE', 'FSPE', 'FSS', 'FSYI', 'FZS', 'GCGI', 'GIMA', 'GRADICSA',
            'HONFCGI', 'HONMINSTRE', 'ICTP', 'ILTM', 'LCGI', 'LFHOM', 'LIAP', 'LIBMS', 'M.INST.SRM', 'MAAV', 'MAPM',
            'MBACP', 'MBASW', 'MBCS', 'MBES', 'MBPS', 'MCGI', 'MCIARB', 'MCIBSE', 'MCIH', 'MCIL', 'MCIM', 'MCIOB',
            'MCIPD', 'MCIPR', 'MCIPS', 'MCIWM', 'MCLIP', 'MCMI', 'MCPARA', 'MCQI', 'MCSD', 'MCSI', 'MEPS', 'MFB',
            'MFHOM', 'MIAP', 'MIBC', 'MIBM', 'MIBMS', 'MIBS', 'MICE', 'MICHEME', 'MIED', 'MIEE', 'MIEEM', 'MIET',
            'MIFA', 'MIFL', 'MIHT', 'MIIE', 'MIIM', 'MIIRSM', 'MIITT', 'MILSA', 'MILT', 'MIMA', 'MIMECHE', 'MINSTCPD',
            'MINSTLM', 'MINSTP', 'MINSTRE', 'MIOD', 'MIOM', 'MIOS', 'MIPLANTE', 'MIRSE', 'MIRTE', 'MISTRUCTE', 'MNI',
            'MRAES', 'MRCA', 'MRCGP', 'MRCP', 'MRCS', 'MRI', 'MRICS', 'MRIN', 'MRPHARMS', 'MRSC', 'MRTPI', 'MSAC',
            'MSHAA', 'MSI', 'MSOE', 'MSPE', 'MSTA', 'MSYI', 'PPRA', 'PRA', 'QG', 'QS', 'RA', 'RFSPE', 'RIBA', 'RMARA',
            'RPP FAPM', 'RPP MAPM', 'SFSPE', 'SIITT', 'TMIET', 'VMSM', 'WS',
            // Armed Forces - http://en.wikipedia.org/wiki/List_of_post-nominal_letters_%28United_Kingdom%29
            'RN', 'RNR', 'RFA', 'RE', 'RAPC', 'RAF', 'RAUXAF', 'RAFVR', 'RAFVR(T)', 'RM', 'RMR', 'RETD'
        );
        $expectedChanges = Array();
        foreach ($titles as $title) {
            $expectedChanges['JOHN SMITH ' . $title] = 'JOHN SMITH';
            $this->names[] = 'JOHN SMITH ' . $title;
        }
        $this->runNamesTest($expectedChanges, 'parse');
    }

}