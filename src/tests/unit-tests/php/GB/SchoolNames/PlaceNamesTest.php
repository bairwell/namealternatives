<?php
/**
 * Tests the Place names code
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Tests\GB\SchoolNames;

/**
 * Test class for place names
 */
class PlaceNamesTest extends \Bairwell\NameAlternatives\Tests\Base
{
    /**
     * @var \Bairwell\NameAlternatives\GB\SchoolNames\PlaceNames $object
     */
    protected $object;

    protected $names = array('HUNCOTE COUNTY PRIMARY SCHOOL', 'LEICS POLYTECHNIC', 'THE LEICS UNI');

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        $this->object = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\SchoolNames\PlaceNames');
        $callBack = function($value)
        {
            switch ($value) {
                case 'HUNCOTE':
                    return Array('HUNCITE', 'HUNCOAT');
                    break;
                case 'LEICS':
                    return Array('LEICESTER', 'LEICESTERSHIRE');
                    break;
                default:
                    return Array();
            }
        };
        $forenamesMock = $this->getMock('\Bairwell\NameAlternatives\GB\PlaceNames', array('parse'));
        $forenamesMock->expects($this->any())
            ->method('parse')
            ->with($this->anything())
            ->will($this->returnCallback($callBack));
        \Bairwell\DI::addLibrary('\Bairwell\NameAlternatives\GB\PlaceNames', $forenamesMock);
        \Bairwell\DI::mocksOnly(TRUE);
    }

    public function testParse()
    {
        $expectedChanges = Array(
            'HUNCOTE COUNTY PRIMARY SCHOOL' => Array('HUNCITE COUNTY PRIMARY SCHOOL', 'HUNCOAT COUNTY PRIMARY SCHOOL'),
            'LEICS POLYTECHNIC' => Array('LEICESTER POLYTECHNIC', 'LEICESTERSHIRE POLYTECHNIC'),
            'THE LEICS UNI' => Array() // Place name is not at the start
        );
        $this->runNamesTest($expectedChanges, 'parse');
    }
}