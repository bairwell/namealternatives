<?php
/**
 * Replaces single occurrences of letters
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Replaces single occurrences of Letters.
 */
class ReplaceSingleOccurrences
{

    /**
     * Replaces single occurrences of letters - but where they may occur multiple times
     *
     * For example, WERECE searching for E replacing with X would result in an array
     * containing WXRECE WERXCE WERECX
     *
     * @param string $name The name we are searching
     * @param string $search The search criteria
     * @param string $replace The replacement criteria
     * @return array An array of matching names (if any)
     */
    public function replace($name, $search, $replace)
    {
        if (is_string($name) === FALSE) {
            throw new \Exception('Name must be a string');
        }
        if (is_string($search) === FALSE) {
            throw new \Exception('Search must be a string');
        }
        if (is_string($replace) === FALSE) {
            throw new \exception('Replace must be a string');
        }
        $return = Array();
        $position = 0;
        while ($position !== FALSE) {
            $position = mb_strpos($name, $search, $position);
            if (is_int($position) === TRUE) {
                $first = mb_substr($name, 0, $position);
                $end = mb_substr($name, ($position + mb_strlen($search)));
                $return[] = $first . $replace . $end;
                $position++;
            }
        }
        $none = str_replace($search, $replace, $name);
        if ($name !== $none && in_array($none, $return) === FALSE) {
            $return[] = $none;
        }
        return $return;
    }

}