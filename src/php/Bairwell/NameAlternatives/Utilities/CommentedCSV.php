<?php
/**
 * CommentedCSV class
 *
 * Loads and processes a # commented CSV file
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Loads and processes a # commented CSV file.
 */
class CommentedCSV
{

    /**
     * Used to convert errors during fopen to Exceptions
     *
     * @param int $errNumber
     * @param string $errString
     * @param string $errFile
     * @param int $errLine
     */
    protected function handleError($errNumber, $errString, $errFile, $errLine)
    {
        throw new \ErrorException($errString, 0, $errNumber, $errFile, $errLine);
    }

    /**
     * Checks a file exists and is readable
     * @param string $filename The file we are checking
     * @return boolean|string True if checks passed, message if not
     */
    protected function checkFile($filename)
    {
        if (is_string($filename) === FALSE) {
            return 'Filename must be a string';
        }
        if (mb_strlen($filename) < 1) {
            return 'Filename cannot be empty';
        }
        if (file_exists($filename) === FALSE) {
            return 'Unable to find file ' . $filename;
        }
        if (is_file($filename) === FALSE) {
            return $filename . ' is not a file';
        }
        if (is_readable($filename) === FALSE) {
            return 'Unable to read file ' . $filename;
        }
        return TRUE;
    }

    /**
     * Loads a # commented csv file
     *
     * @throws \Exception
     * @param string $filename The name of the file
     * @param string $delimiter The delimiter of the csv (usually comma)
     * @return array An array of the lines
     */
    public function loadCommentedCSV($filename, $delimiter = ',')
    {
        $checkResults = $this->checkFile($filename);
        if ($checkResults !== TRUE) {
            throw new \Exception($checkResults);
        }
        $array = Array();
        /**
         * Use our error handler
         */
        set_error_handler(array($this, 'handleError'));
        $handle = fopen($filename, 'r');
        /**
         * Now restore the normal error handler
         */
        restore_error_handler();
        $buffer = fgets($handle, 4096);
        while ($buffer !== FALSE) {
            if ($buffer !== NULL && $buffer[0] !== '#' && mb_strlen($buffer) > 3) {
                $line = str_getcsv($buffer, $delimiter);
                $array[] = $line;
            }
            $buffer = fgets($handle, 4096);
        }
        fclose($handle);
        return $array;
    }

    /**
     * Searches for matches within a CSV file
     *
     * @param string $filename The full filename/path to look for
     * @param string $nameToMatch The name we are trying to match
     * @param string $delimiter  The delimiter of the csv (usually comma)
     * @return array An array of matching names
     */
    public function searchForCSVmatches($filename, $nameToMatch, $delimiter = ',')
    {
        $return = Array();
        $csv = $this->loadCommentedCSV($filename, $delimiter);
        foreach ($csv as $line) {
            foreach ($line as $name) {
                if (mb_strlen($name) > 0) {
                    $quoted = preg_quote($name);
                    if (preg_match('/^(.*[ ,\.\(\[]|)(' . $quoted . ')([ ,\.\)\]].*|)$/', $nameToMatch, $matches)===1) {
                        /**
                         * We've found a match in our inbound string
                         */
                        foreach ($line as $word) {
                            $return[] = $matches[1] . $word . $matches[3];
                        }
                    }
                }
            }
        }
        $toReturn = Array();
        foreach ($return as $newName) {
            if ($newName !== $nameToMatch && in_array($newName, $toReturn) === FALSE) {
                $toReturn[] = $newName;
            }
        }
        return $toReturn;
    }

}