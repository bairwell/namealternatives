<?php
/**
 * Expands prefixes class
 *
 * Expands name prefixes
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Expands name prefixes.
 */
class ExpandPrefixes
{
    /**
     * Checks a name against a prefix and a list of alternatives and expands where appropriate
     *
     * @param string $prefix The main prefix we are looking for
     * @param string $name The name we are checking against
     * @param array $alternatives The list of alternative prefixes
     * @param array $return The names we have gathered so far
     * @return array The names we have gathered so far and any new ones
     */
    public function expand($prefix, $name, $alternatives, $return)
    {
        if (is_string($prefix) === FALSE) {
            throw new \Exception('Prefix needs to be a string');
        }
        if (is_string($name) === FALSE) {
            throw new \Exception('Name needs to be a string');
        }
        if (is_array($alternatives) === FALSE) {
            throw new \Exception('Alternatives needs to be an array');
        }
        if (preg_match('/( |^)' . preg_quote($prefix) . '([A-Z\'+])/', $name) === 1) {
            $return = $this->prefixMatches($prefix, $name, $alternatives, $return);
        } else {
            $return = $this->checkAlternatives($prefix, $name, $alternatives, $return);
        }
        return $return;
    }

    /**
     * Checks to see if any of the alternatives match
     *
     * @param string $prefix The main prefix we are looking for
     * @param string $name The name we are checking against
     * @param array $alternatives The list of alternative prefixes
     * @param array $return The names we have gathered so far
     * @return array The names we have gathered so far and any new ones
     */
    protected function checkAlternatives($prefix, $name, $alternatives, $return)
    {
        foreach ($alternatives as $alt) {
            if (preg_match('/( |^)' . preg_quote($alt) . '([A-Z\'+])/', $name) === 1) {
                $new = preg_replace(
                    '/( |^)' . preg_quote($alt) . '([A-Z\'+])/',
                    '\1' . preg_quote($prefix) . '\2',
                    $name
                );
                if (($new !== $name) && (in_array($new, $return) === FALSE)) {
                    $return[] = $new;
                }
                foreach ($alternatives as $alt2) {
                    $new = preg_replace(
                        '/( |^)' . preg_quote($alt) . '([A-Z\'+])/',
                        '\1' . preg_quote($alt2) . '\2',
                        $name
                    );
                    if (($new !== $name) && (in_array($new, $return) === FALSE)) {
                        $return[] = $new;
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Expand the alternatives as the prefix matches
     *
     * @param string $prefix The main prefix we are looking for
     * @param string $name The name we are checking against
     * @param array $alternatives The list of alternative prefixes
     * @param array $return The names we have gathered so far
     * @return array The names we have gathered so far and any new ones
     */
    protected function prefixMatches($prefix, $name, $alternatives, $return)
    {
        foreach ($alternatives as $alt) {
            $new = preg_replace(
                '/( |^)' . preg_quote($prefix) . '([A-Z\'+])/',
                '\1' . preg_quote($alt) . '\2',
                $name
            );
            if (($new !== $name) && (in_array($new, $return) === FALSE)) {
                $return[] = $new;
            }
        }
        return $return;
    }
}