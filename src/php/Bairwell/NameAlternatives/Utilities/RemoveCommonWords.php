<?php
/**
 * Remove common words
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Removes common words.
 */
class RemoveCommonWords
{
    /**
     * Removes common words from a string
     * @param string $name The name/string we are working on
     * @param array $words The words we are looking at removing
     * @param string $firstSeparator The regular expression to identify the start of the words. Default: '( |^)'
     * @param string $secondSeparator The regular expression to identify the end of the words. Default:'( |$)'
     * @return array A list of strings with the words removed
     */
    public function remove($name, $words, $firstSeparator = NULL, $secondSeparator = NULL)
    {
        if (is_string($name) === FALSE) {
            throw new \Exception('Name must be a string');
        }
        if (is_array($words) === FALSE) {
            throw new \Exception('Words must be an array');
        }
        if ($firstSeparator === NULL) {
            $firstSeparator = '( |^)';
        }
        if (is_string($firstSeparator) === FALSE) {
            throw new \Exception('First separator must be a string or null');
        }
        if ($secondSeparator === NULL) {
            $secondSeparator = '( |$)';
        }
        if (is_string($secondSeparator) === FALSE) {
            throw new \Exception('Second separator must be a string or null');
        }
        usort($words, Array($this, 'sortByLength'));
        $return = Array($name);
        foreach ($words as $word) {
            $return = $this->doMatches($word, $return, $firstSeparator, $secondSeparator);
        }
        /**
         * Remove the first one we put in
         */
        unset($return[0]);
        return $return;
    }

    /**
     * Actually perform the word matches for removal
     *
     * @param string $word The word we are currently looking for
     * @param array $return The list of words we currently have
     * @param $firstSeparator The first separator
     * @param $secondSeparator The second separator
     * @return array The list of words
     */
    protected function doMatches($word, $return, $firstSeparator, $secondSeparator)
    {
        foreach ($return as $currentName) {
            $escaped = preg_quote($word);
            $pregExp = '/' . $firstSeparator . '(' . $escaped . '|\(' . $escaped . '\))' . $secondSeparator . '/';
            if (preg_match($pregExp, $currentName) === 1) {
                $new = preg_replace($pregExp, '\\3', $currentName);
                $new = trim(str_replace('  ', ' ', $new));
                if (in_array($new, $return) === FALSE) {
                    $return[] = $new;
                }
            }
        }
        return $return;
    }

    /**
     * Sorts strings by length for use in http://uk3.php.net/usort
     *
     * @param string $a The first string
     * @param string $b The second string
     * @return int 0 if same, <0 if first less, >1 first greater than second
     */
    protected function sortByLength($a, $b)
    {
        return (strlen($b) - strlen($a));
    }
}