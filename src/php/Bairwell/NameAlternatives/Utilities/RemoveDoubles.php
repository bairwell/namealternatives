<?php
/**
 * Removes doubled letters from strings
 *
 * Expands And symbols
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Removes Double Letters from strings.
 */
class RemoveDoubles implements \Bairwell\NameAlternatives\ParserInterface
{
    /**
     * Removes doubled characters from a string
     *
     * @param string $name The string
     * @return array Strings with the doubled characters removed
     */
    public function parse($name)
    {
        if (is_string($name) === FALSE || mb_strlen($name) < 1) {
            return array();
        }
        $toCheck = array($name);
        $return = array();
        $count = count($toCheck);
        while ($count > 0) {
            $checking = array_pop($toCheck);
            if (in_array($checking, $return) === FALSE && $name !== $checking) {
                $return[] = $checking;
            }
            if (preg_match('{(.)\1+}', $checking) === 1) {
                $toCheck = array_merge($toCheck, $this->processADouble($checking));
            }
            $count = count($toCheck);
        }

        return $return;
    }

    /**
     * Returns an array of strings of which only one double combination has
     * been taken out of $original at any time
     *
     * @param string $original The original string
     * @return array Multiple matches if applicable
     */
    protected function processADouble($original)
    {
        $strlen = mb_strlen($original);
        $lastchar = NULL;
        $i = 0;
        $stringSoFar = '';
        $new = Array();
        /**
         * Loop through the string looking for characters which
         * are identical to the previous one
         */
        while ($i <= $strlen) {
            $char = mb_substr($original, $i, 1);
            if ($char !== $lastchar) {
                $stringSoFar .= $char;
            } else {
                $newString = $stringSoFar . mb_substr($original, ($i + 1));
                $new[] = $newString;
                $stringSoFar .= $char;
            }
            $lastchar = $char;
            $i++;
        }
        return $new;
    }

}