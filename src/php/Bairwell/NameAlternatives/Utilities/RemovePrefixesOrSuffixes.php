<?php
/**
 * Removes prefixes or suffixes from a name.
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Removes prefixes or suffixes.
 */
class RemovePrefixesOrSuffixes
{

    /**
     * Removes prefixes
     *
     * @param string $name The name
     * @param array $exts The list of prefixes to remove
     * @return array A name if matched, with the prefix removed
     */
    public function removePrefixes($name, $exts)
    {
        return $this->remove($name, $exts, TRUE);
    }

    /**
     * Removes suffixes
     *
     * @param string $name The name
     * @param array $exts The list of suffix to remove
     * @return array A name if matched, with the suffix removed
     */
    public function removeSuffixes($name, $exts)
    {
        return $this->remove($name, $exts, FALSE);
    }

    /**
     * Removes prefixes or suffixes
     *
     * @param string $name The name
     * @param array $exts The list of prefixes or suffixes
     * @param bool $prefixes TRUE=Treat as prefixes, FALSE=treat as suffixes
     * @return array Names if matched, with the prefix removed
     */
    protected function remove($name, $exts, $prefixes = TRUE)
    {
        if (is_string($name) === FALSE) {
            throw new \Exception('Name must be a string');
        }
        if (is_array($exts) === FALSE) {
            throw new \Exception('Exts must be an array');
        }
        usort($exts, Array($this, 'sortByLength'));
        $name = preg_replace('/[^A-Z \(\)]/', '', $name);
        $originalName = $name;
        $matched = TRUE;
        while ($matched === TRUE) {
            $matched = FALSE;
            foreach ($exts as $item) {
                $length = mb_strlen($item);
                if ($prefixes === TRUE) {
                    $substr = mb_substr($name, 0, ($length + 1));
                    if ($substr === $item . ' ') {
                        $name = mb_substr($name, ($length + 1));
                        $matched = TRUE;
                    }
                } else {
                    $substr = mb_substr($name, -($length + 1));
                    if ($substr === ' ' . $item) {
                        $name = mb_substr($name, 0, -($length + 1));
                        $matched = TRUE;
                    }
                }
            }
        }
        $return = Array();
        if ($name !== $originalName) {
            $return[] = $name;
        }
        return $return;
    }

    /**
     * Sorts strings by length for use in http://uk3.php.net/usort
     *
     * @param string $a The first string
     * @param string $b The second string
     * @return int 0 if same, <0 if first less, >1 first greater than second
     */
    protected function sortByLength($a, $b)
    {
        return (strlen($b) - strlen($a));
    }
}