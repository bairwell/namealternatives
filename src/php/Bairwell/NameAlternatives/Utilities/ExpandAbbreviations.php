<?php
/**
 * Expands abbreviations and initialisms
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\Utilities;

/**
 * Expands abbreviations.
 */
class ExpandAbbreviations
{
    /**
     * Expands abbreviations
     *
     * @param string $name The string/name we are looking at expanding
     * @param array $abbreviations The list of abbreviations
     * @return array An list of strings with the abbreviations expanded
     */
    public function expand($name, $abbreviations)
    {
        if (is_string($name) === FALSE) {
            throw new \Exception('Original name needs to be a string');
        }
        if (is_array($abbreviations) === FALSE) {
            throw new \Exception('Abbreviations needs to be an array');
        }
        $return = Array($name);

        foreach ($abbreviations as $abbr => $extend) {
            foreach ($return as $currentName) {
                $return = $this->checkSingleName($currentName, $abbr, $extend, $return);
            }
        }
        /**
         * Remove the first one we put in
         */
        unset($return[0]);
        return $return;
    }

    /**
     * Checks a single name
     * @param string $currentName
     * @param string $abbr The abbreviation we are checking
     * @param array $extend The full extensions of the abbreviations
     * @param array $return The names
     * @return array The returned names
     */
    protected function checkSingleName($currentName, $abbr, $extend, $return)
    {
        if (preg_match('/( |^)' . $abbr . '( |$)/', $currentName) === 1) {
            $return = $this->checkAndAdd($abbr, $extend, $currentName, $return);
        } else {
            if (is_string($extend) === TRUE && (preg_match('/( |^)' . $extend . '( |$)/', $currentName) === 1)) {
                $return[] = preg_replace('/( |^)' . $extend . '( |$)/', '\\1' . $abbr . '\\2', $currentName);
            } else {
                if (is_array($extend) === TRUE) {
                    foreach ($extend as $extended) {
                        $return = $this->checkAndAdd($extended, $abbr, $currentName, $return);
                    }
                }
            }
        }
        return $return;
    }

    /**
     * Actually expand the abbreviations and add them to our array
     * @param string $abbr The abbreviation we are checking
     * @param array|string $extend The array or string we should replace the abbreviation with
     * @param string $name The original name
     * @param array $return The list of names we have compiled so far
     * @return array The list of names
     */
    protected function checkAndAdd($abbr, $extend, $name, $return)
    {
        if (is_string($extend) === TRUE) {
            $new = preg_replace('/( |^)' . $abbr . '( |$)/', '\\1' . $extend . '\\2', $name);
            if (in_array($new, $return) === FALSE && $new !== $name) {
                $return[] = $new;
            }
        } else if (is_array($extend) === TRUE) {
            foreach ($extend as $extended) {
                $new = preg_replace('/( |^)' . $abbr . '( |$)/', '\\1' . $extended . '\\2', $name);
                if (in_array($new, $return) === FALSE && $new !== $name) {
                    $return[] = $new;
                }
            }
        }
        return $return;
    }
}