<?php
/**
 * The overview abstract which is extended by each of the overview methods
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives;

/**
 * The overview abstract which is extended by each of the overview methods.
 */
abstract class OverviewAbstract implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * The list of modules that should be processed
     * @return array
     */
    abstract public function getModules();

    /**
     * Actually do the parsing
     * @param string $name The name we are processing
     * @return array The list of names we have matched
     */
    public function parse($name)
    {
        $modules = $this->getModules();
        /**
         * @var \Bairwell\NameAlternatives\Utilities\Ascii $ascii
         */
        $ascii = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\Ascii');
        $name = $ascii->parse($name);
        $names = Array($name[0]);
        $wordsProcessedByModules = Array();
        foreach ($modules as $module) {
            $wordsProcessedByModules[$module] = Array();
        }
        foreach ($modules as $module) {
            /**
             * @var \Bairwell\NameAlternatives\ParserInterface $lib
             */
            $lib = \Bairwell\DI::getLibrary($module);
            foreach ($names as $processingName) {
                $this->parseModuleName($lib, $module, $names, $processingName, $wordsProcessedByModules);
            }
        }
        return $names;
    }

    /**
     * Pass a name to a module for parsing
     * @param \Bairwell\NameAlternatives\ParserInterface $lib The module we are handling
     * @param string $module The name of the module we are working on
     * @param array $names The names we have compiled so far
     * @param name $processingName What name are we currently processing
     * @param array $wordsProcessedByModules The list of words processed by each module
     */
    protected function parseModuleName(\Bairwell\NameAlternatives\ParserInterface $lib, $module, &$names, $processingName, &$wordsProcessedByModules)
    {
        if (in_array($processingName, $wordsProcessedByModules[$module]) === FALSE) {
            $newNames = $lib->parse($processingName);
            if (count($newNames) > 0) {
                foreach ($newNames as $newName) {
                    if (in_array($newName, $names) === FALSE) {
                        $names[] = $newName;
                        $wordsProcessedByModules[$module][] = $newName;
                    }
                }
            }
            $wordsProcessedByModules[$module][] = $processingName;
        }
    }


}