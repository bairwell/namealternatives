<?php
/**
 * Replaces "PH" s in names with Fs and vice versa
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\Utilities;

/**
 * Replaces "PH" s in names with Fs and vice versa.
 */
class PhF implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Converts PH to V and vice versa
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        /**
         * @var \Bairwell\NameAlternatives\Utilities\ReplaceSingleOccurrences
         */
        $replace = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ReplaceSingleOccurrences');
        $phV = $replace->replace($name, 'PH', 'F');
        $vPh = $replace->replace($name, 'F', 'PH');
        return array_merge($phV, $vPh);
    }

}
