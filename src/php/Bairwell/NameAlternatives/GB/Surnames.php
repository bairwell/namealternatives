<?php
/**
 * GB Surnames
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB;

/**
 * Surnames.
 */
class Surnames extends \Bairwell\NameAlternatives\OverviewAbstract
{

    /**
     * The list of modules that should be processed
     * @return array
     */
    public function getModules()
    {
        return Array(
            '\Bairwell\NameAlternatives\GB\Surnames\DifferentVowelStarts',
            '\Bairwell\NameAlternatives\GB\Surnames\IsleOfWight',
            '\Bairwell\NameAlternatives\GB\Surnames\ParliamentaryParser',
            '\Bairwell\NameAlternatives\GB\Surnames\Prefixes',
            '\Bairwell\NameAlternatives\GB\Surnames\RChiswell',
            '\Bairwell\NameAlternatives\GB\Surnames\SilentHAsSecond',
            '\Bairwell\NameAlternatives\GB\Surnames\SilentLettersAtEnd',
            '\Bairwell\NameAlternatives\GB\Surnames\Suffixes',
            '\Bairwell\NameAlternatives\GB\Surnames\Wikipedia',
            '\Bairwell\NameAlternatives\GB\Surnames\Wingops',
            '\Bairwell\NameAlternatives\GB\Utilities\CkX',
            '\Bairwell\NameAlternatives\GB\Utilities\PhF',
            '\Bairwell\NameAlternatives\GB\Utilities\PhV',
            '\Bairwell\NameAlternatives\GB\Utilities\PluralsAndPossessions'
        );
    }


}