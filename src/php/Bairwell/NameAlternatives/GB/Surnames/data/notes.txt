When including people's email addresses, please only include the internet domain to help prevent spam

Not contacted:

Contacted, but not responded:
http://www.ingeneas.com/alternate.html
    Who Contacted: Contact (@ingeneas.com)
    When Contacted: 6th October 2011
    Contacted by Who: Richard Chiswell
    
Refused:

Granted:
http://www.isle-of-wight-fhs.co.uk/bmd/surn.htm
    File: Isleofwight.csv
    Granted by: Geoff Allan (@isle-of-wight-fhs.co.uk)
    Date: 6th October 2011
    To: Richard Chiswell

http://www.wing-ops.org.uk/spellings.html
    Fine: wingops.csv
    Granted by: Alex (@wing-ops.org.uk)
    Date: 6th October 2011
    To: Richard Chiswell

http://ukparse.kforge.net/svn/parlparse/members/member-aliases.xml
    File: parlimentaryparser.csv
    Creative Commons licenced via Publicwhip and other sources
