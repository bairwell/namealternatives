<?php
/**
 * Removes/expands common prefixes
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\Surnames;

/**
 * Removes/expands common prefixes.
 */
class Prefixes implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Removes/expands common prefixes
     *
     * @param string $name The upper cased surname
     * @return array An array of any alternatives
     */
    public function parse($name)
    {
        $return = Array();
        $prefixes = Array(
            'MC' => Array('MAC'),
            'O\'' => NULL,
            'O' => NULL,
            'ROW' => Array('RO', 'ROE'),
            'RO' => Array('ROW', 'ROE'),
            'ROE' => Array('ROW', 'RO')
        );
        /**
         * @var \Bairwell\NameAlternatives\Utilities\ExpandPrefixes $expandPrefixes
         */
        $expandPrefixes = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ExpandPrefixes');
        foreach ($prefixes as $prefix => $value) {
            if (is_array($value) === TRUE) {
                $return = $expandPrefixes->expand($prefix, $name, $value, $return);
            } else {
                if ($value === NULL) {
                    $matches = Array();
                    if (preg_match('/( |^)' . $prefix . '(.*)$/', $name, $matches) === 1) {
                        $new = $matches[1] . $matches[2];
                        if (($new !== $name) && (in_array($new, $return) === FALSE)) {
                            $return[] = $new;
                        }
                    }
                }
            }
        }
        return $return;
    }

}
