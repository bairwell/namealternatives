<?php
/**
 * Adds/removes a H as the second letter of the surname
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\Surnames;

/**
 * Adds/removes a H as the second letter of the surname.
 */
class SilentHAsSecond implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Adds/removes a H as the second letter of the surname
     *
     * @param string $name The upper cased surname
     * @return array An array of any alternatives
     */
    public function parse($name)
    {
        $return = Array();
        if (mb_substr($name, 1, 1) === 'H') {
            $return[] = mb_substr($name, 0, 1) . mb_substr($name, 2);
        } else {
            $return[] = mb_substr($name, 0, 1) . 'H' . mb_substr($name, 1);
        }
        return $return;
    }

}
