<?php
/**
 * Adds a silent H to the start of surnames that begin with a vowel, removes the H
 * if the second letter is a vowel
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\Surnames;

/**
 * Adds H to start of words that start with vowel. Removes H from start if second letter is H.
 */
class SilentHAtStart implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Adds a silent H to the start of surnames that begin with a vowel, removes the H
     * if the second letter is a vowel
     *
     * @param string $name The upper cased surname
     * @return array An array of any alternatives
     */
    public function parse($name)
    {
        $vowels = Array('A', 'E', 'I', 'O', 'U');
        $first = mb_substr($name, 0, 1);
        $return = Array();
        if (in_array($first, $vowels) === TRUE) {
            $return[] = 'H' . $name;
        } else {
            if ($first === 'H') {
                $second = mb_substr($name, 1, 1);
                if (in_array($second, $vowels) === TRUE) {
                    $return[] = mb_substr($name, 1);
                }
            }
        }
        return $return;
    }

}
