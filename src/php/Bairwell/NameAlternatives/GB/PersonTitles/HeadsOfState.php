<?php
/**
 * Removes heads of state person titles
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\PersonTitles;

/**
 * Matches heads of state people titles against a list.
 *
 * Abstracted from
 *  http://en.wikipedia.org/wiki/Titles
 */
class HeadsOfState implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Matches heads of state people titles against a list abstracted from
     * http://en.wikipedia.org/wiki/Titles
     *
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $titles = Array(
            'CHAIRMAN', 'VICE CHAIRMAN', 'COLONEL', 'LIEUTENANT COLONEL', 'PONTIFF', 'PRESIDENT', 'DEPUTY PRESIDENT',
            'EXECUTIVE VICE PRESIDENT', 'LORD PRESIDENT OF THE COUNCIL', 'VICE PRESIDENT', 'REGENT', 'CAPTAINS REGENT',
            'PRINCE REGENT', 'GREAT LEADER', 'DEAR LEADER'
        );
        /**
         * @var \Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes $remove
         */
        $remove = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\RemovePrefixesOrSuffixes');
        return $remove->removePrefixes($name, $titles);
    }

}
