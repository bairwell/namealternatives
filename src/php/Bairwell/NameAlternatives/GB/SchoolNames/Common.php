<?php
/**
 * Remove common words from the school name
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\SchoolNames;

/**
 * Remove common words from the school name.
 */
class Common implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Removes common words from the school name
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $common = Array(
            '1118', 'A BUSINESS ENTERPRISE', 'BUSINESS AND ENTERPRISE', 'SPECIALIST COLLEGE', 'SPECIALIST SPORTS',
            'LANGUAGE COLLEGE', 'APPLIED LEARNING', 'COMMUNITY SCHOOL', 'MATHEMATICS AND COMPUTING',
            'VOLUNTARY AIDED', 'CATHOLIC SCHOOL', 'SPECIALIST SCHOOL', 'COMMUNITY SCHOOL', 'GRAMMAR SCHOOL',
            'ARTS COLLEGE', 'SPORTS COLLEGE', 'TECHNOLOGY COLLEGE', 'COMMUNITY TECHNOLOGY', 'THE', 'GRAMMAR SCHOOL',
            'HIGH SCHOOL', 'MIDDLE SCHOOL', 'SCHOOL', 'COMPREHENSIVE', 'SECONDARY', 'LANGUAGE COLLEGE',
            'TECHNOLOGY COLLEGE', 'PERFORMING ARTS', 'ENTERPRISE COLLEGE', 'ROMAN CATHOLIC', 'CHURCH OF ENGLAND',
            'FOR GIRLS', 'FOR BOYS', 'BOYS', 'GIRLS', 'RC', 'GRAMMAR', 'COLLEGE', 'ELEMENTARY', 'COUNTY', 'PRIMARY',
            'UNIVERSITY', 'SCHOOLS', 'COMMUNITY', 'INFANT', 'JUNIOR', 'OF', 'NURSERY', 'AND', 'INFANT AND NURSERY',
            'JUNIOR AND INFANT', 'JUNIOR, INFANT AND NURSERY', 'VOLUNTARY AIDED', 'LOWER', 'UPPER', 'PRE SCHOOL',
            'COMMUNITY DAY', 'PRESCHOOL', 'PRE-SCHOOL'

        );
        /**
         * @var \Bairwell\NameAlternatives\Utilities\RemoveCommonWords $removeCommon
         */
        $removeCommon = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\RemoveCommonWords');
        return $removeCommon->remove($name, $common);
    }
}
