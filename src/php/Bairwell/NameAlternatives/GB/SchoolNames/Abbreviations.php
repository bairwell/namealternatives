<?php
/**
 * NameAlternatives -> GB -> School Names -> Abbreviations
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\SchoolNames;

/**
 * Expand abbreviations in school names.
 */
class Abbreviations implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Expand abbreviations in school names
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $abbreviations = Array(
            'ST' => 'SAINT',
            'COFE' => 'CHURCH OF ENGLAND',
            'COE' => 'CHURCH OF ENGLAND',
            'TECH' => 'TECHNOLOGY',
            'RC' => 'ROMAN CATHOLIC',
            'MATHS' => Array('MATHEMATICS', 'MATH'),
            'ICT' => 'INFORMATION AND COMMUNICATION TECHNOLOGIES',
            'VA' => 'VOLUNTARY AIDED',
            'FE' => 'FURTHER EDUCATION',
            'FCJ' => 'FAITHFUL COMPANIONS OF JESUS SISTERS',
            'UCH' => 'UNIVERSITY COLLEGE HOSPITAL',
            'SEN' => 'SPECIAL EDUCATIONAL NEEDS',
            'KS4' => 'KEY STAGE 4',
            'MLD' => 'MODERATE LEARNING DIFFICULTIES',
            'VC' => 'VOLUNTARY CONTROLLED',
            'PRU' => 'PUPIL REFERRAL UNIT',
            'SILC' => 'SPECIALIST LEARNING CENTRE'
        );
        /**
         * @var \Bairwell\NameAlternatives\Utilities\ExpandAbbreviations $abbrevs
         */
        $abbrevs = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\Utilities\ExpandAbbreviations');
        return $abbrevs->expand($name, $abbreviations);
    }
}
