<?php
/**
 * Check for names that looks like Saint's names and parse them as a forename to a single level
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\PlaceNames;

/**
 * Check for names that looks like Saint's names and parse them as a forename to a single level.
 */
class Saints implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Check for names that looks like Saint's names and parse them as a forename to a single level
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $words = str_word_count($name, 1);
        $wordCount = count($words);
        $return = Array();
        $saints = Array('ST', 'SAINT', 'ST.');
        $previousWordSaint = FALSE;
        $currentWord = 0;
        foreach ($words as $word) {
            $currentWord++;
            if (in_array($word, $saints)===TRUE) {
                if ($wordCount > 1 && $currentWord === $wordCount) {
                    /**
                     * St cannot be the last word in a sentence
                     */
                    $return = $this->addWord($return, $word);
                    $previousWordSaint = FALSE;

                } else {
                    $return = $this->addWord($return, $saints);
                    $previousWordSaint = TRUE;
                }
            } else {
                if ($previousWordSaint === TRUE) {
                    $return = $this->possibleName($return, $word);
                } else {
                    $return = $this->addWord($return, $word);
                }
                $previousWordSaint = FALSE;
            }
        }
        $toReturn = Array();
        foreach ($return as $newName) {
            if ($newName !== $name && in_array($newName, $toReturn) === FALSE) {
                $toReturn[] = $newName;
            }
        }
        return $toReturn;
    }

    protected function possibleName($existingList, $possibleName)
    {
        /**
         * @var Bairwell\NameAlternatives\GB\Forenames $forenames
         */
        $forenames = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\Forenames');
        /**
         * @var \Bairwell\NameAlternatives\GB\Utilities\PluralsAndPossessions $plurals
         */
        $plurals = \Bairwell\DI::getLibrary('\Bairwell\NameAlternatives\GB\Utilities\PluralsAndPossessions');
        /**
         * Remove any pluralisations
         */
        $dePluraled = $plurals->parse($possibleName);
        $dePluraled[] = $possibleName;
        $extractForenames = Array();
        /**
         * Extract the forenames
         */
        foreach ($dePluraled as $newWord) {
            $extractForenames[] = $newWord;
            $newForenames = $forenames->parse($newWord);
            $extractForenames = array_merge($newForenames, $extractForenames);
        }
        $pluraledNames = Array();
        foreach ($extractForenames as $forename) {
            $pluraledNames[] = $forename;
            $pluraledNames[] = $forename . 'S';
            $pluraledNames[] = $forename . '\'S';
            $pluraledNames[] = $forename . 'S\'';
        }
        $return = $this->addWord($existingList, $pluraledNames);
        return $return;
    }

    /**
     * Adds a word to our list
     * @param array $existingList The existing wordlist
     * @param string $newWord The new word we are adding
     * @return array
     */
    protected function addWord($existingList, $newWord)
    {
        $newList = Array();
        /**
         * Save checking multiple times
         */
        $isArray = is_array($newWord);
        if (count($existingList) === 0) {
            if ($isArray === TRUE) {
                foreach ($newWord as $word) {
                    $newList[] = $word;
                }
            } else {
                $newList[] = $newWord;
            }
        } else {
            foreach ($existingList as $existingWords) {
                if ($isArray === TRUE) {
                    foreach ($newWord as $word) {
                        $newList[] = $existingWords . ' ' . $word;
                    }
                } else {
                    $newList[] = $existingWords . ' ' . $newWord;
                }
            }
        }
        return $newList;
    }
}
