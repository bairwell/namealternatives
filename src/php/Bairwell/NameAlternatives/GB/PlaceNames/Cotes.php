<?php
/**
 * Replaces the misspellings/alternative spellings of COTE, CITE and COTE
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\PlaceNames;

/**
 * Replaces the misspellings/alternative spellings of COTE, CITE and COTE.
 */
class Cotes implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Replaces the misspellings/alternative spellings of COTE, CITE and COTE
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $return = Array();
        $matches = Array();
        if (preg_match('/(.*)(COTE|CITE|COAT)( .*|$)/', $name, $matches) === 1) {
            $subs = Array('COTE', 'CITE', 'COAT');
            foreach ($subs as $sub) {
                $newName = $matches[1] . $sub . $matches[3];
                if ($newName !== $name) {
                    $return[] = $newName;
                }
            }
        }
        return $return;
    }
}
