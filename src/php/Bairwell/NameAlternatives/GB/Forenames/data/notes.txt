When including people's email addresses, please only include the internet domain to help prevent spam

Not contacted:
http://usefulenglish.ru/vocabulary/mens-names
http://usefulenglish.ru/vocabulary/womens-names
http://www.cc.kyoto-su.ac.jp/~trobb/nicklist.html
http://www.whatsinaname.net/
http://www.tngenweb.org/franklin/nickinter.htm

Contacted, but not responded:

http://genealogy.about.com/library/bl_nicknames.htm
    Who Contacted: Kimberly (@about.com)
    When Contacted: 6th October 2011
    Contacted by Who: Richard Chiswell
    
Refused:

Granted:
http://ukparse.kforge.net/svn/parlparse/members/member-aliases.xml
    File: parlimentaryparser.csv
    Creative Commons licenced via Publicwhip and other sources

http://code.google.com/p/nickname-and-diminutive-names-lookup/
    File: Carlton.csv
    Creative Commons 3.0 BY licenced

http://www.nireland.com/anne.johnston/Diminutives.htm
    File: nireland.csv
    Granted by: Anne Johnston (@nireland.com)
    Date: 10th October 2011
    To: Richard Chiswell