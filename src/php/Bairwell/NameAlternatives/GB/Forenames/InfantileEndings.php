<?php
/**
 * Changes names ending IE,E,Y and vice versa
 *
 * This work is licensed under the MIT License
 * Copyright (c) 2011 Bairwell Ltd
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this
 * software and associated documentation files (the "Software"), to deal in the Software
 * without restriction, including without limitation the rights to use, copy, modify, merge,
 * publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons
 * to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or
 * substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 * BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * PHP Version 5
 *
 * @package Bairwell
 * @subpackage NameAlternatives
 * @author Richard Chiswell <richard@bairwell.com>
 * @copyright 2011 Bairwell Ltd
 * @license MIT
 */
namespace Bairwell\NameAlternatives\GB\Forenames;

/**
 * Changes names ending IE,E,Y and vice versa.
 */
class InfantileEndings implements \Bairwell\NameAlternatives\ParserInterface
{

    /**
     * Changes names ending IE,E,Y and vice versa
     * @param string $name The name we are looking for
     * @return array An array of matching names
     */
    public function parse($name)
    {
        $return = Array();
        $endings = Array('IE', 'E', 'Y');
        $matches = Array();
        if (preg_match('/(IE|Y)$/', $name, $matches) === 1) {
            $withoutEnding = mb_substr($name, 0, -mb_strlen($matches[1]));
            foreach ($endings as $ending) {
                $new = $withoutEnding . $ending;
                if ($new !== $name) {
                    $return[] = $new;
                }
            }
        } else {
            if (preg_match('/[^I]E$/', $name) === 1) {
                $withoutEnding = mb_substr($name, 0, -1);
                foreach ($endings as $ending) {
                    $new = $withoutEnding . $ending;
                    if ($new !== $name) {
                        $return[] = $new;
                    }
                }
            }
        }
        return $return;
    }

}
