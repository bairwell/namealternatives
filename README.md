NameAlternatives
================
This a module written in PHP 5.3.1+ (and, therefore, requires Namespaces support) which given a forename,
surname, school name, place name or person's full name/title will try and produce data suitable for feeding
into a search system such as Apache's Lucene.

For example, if you were to give it the forename "Richard", it will generate list of names such as "RICHIE",
"DICK", "RICARDO", "DICKIE", "RICHY" etc. If these are fed into a search system and then someone searches
for "Dick", they'll get the record for "Richard" (as you would expect). Most (if not all) freely available
search systems cannot make associations like this. It also knows that the place name "Brum" is for
"Birmingham".

Along with straight name matchings, it does some automated magic on the names: for example, place names
will have "SHIRE" stripped from the end, school names will have "FE" expanded to be "Further Education"
etc etc - so you can get some quite interesting results.

The system only currently knows about GB (Great Britain/UK) style variants in names - so it'll only be
any good for matching names in the UK.

Copyright
=========
This module was originally written by Richard Chiswell of Bairwell Ltd. Whilst Bairwell Ltd
holds the copyright on this work, we have licenced it under the MIT licence.

Bairwell Ltd: [http://www.bairwell.com](http://www.bairwell.com) / Twitter: [http://twitter.com/bairwell](http://twitter.com/bairwell)
Richard Chiswell: [http://blog.rac.me.uk](http://blog.rac.me.uk) / Twitter: [http://twitter.com/rchiswell](http://twitter.com/rchiswell)

Installation
============
The easiest way to install this module is to install it using the [PEAR Installer](http://pear.bairwell.com).
This installer is the PHP community's de-facto standard for installing PHP components.

    sudo pear channel-discover http://pear.bairwell.com
    sudo pear install --alldeps http://pear.bairwell.com/Bairwell_NameAlternatives

Or to install the dist/Bairwell_NameAlternatives...tgz file
    pear install dist/Bairwell_NameAlternatives*.tgz

Usage
=====
$alternatives = \Bairwell\NameAlternatives\Forenames::parse('Richard');

The best documentation for Bairwell_NameAlternatives are the unit tests, which are shipped in the package.

There is also a NameAlternativesDemonstration.php file under www/ .

You will find them installed into your PEAR repository, which on Linux systems is normally /usr/share/php/test.
Documentation in the source code is included in build/docblox .

To build
========
This is a [PHIX](http://phix-project.org) compatible module and if you have PHIX installed you
can just do:

    phing test          <- to run the unit tests
    phing code-review   <- get code quality information
    phing phpdoc        <- get PhpDocumentor docs
    phing pear-package  <- to generate the package

This module has also been built with the [Jenkins CI](http://www.jenkins-ci.org) in mind and
you can use the build.jenkins.xml file to run the tests and update Jenkins.


As A Dependency On Your Component
=================================
If you are creating a component that relies on Bairwell_NameAlternatives, please make sure that you add
Bairwell_NameAlternatives to your component's package.xml file:

    <dependencies>
      <required>
        <package>
          <name>Bairwell_DI</name>
          <channel>pear.bairwell.com</channel>
          <min>0.0.1</min>
          <max>0.999.999</max>
        </package>
      </required>
    </dependencies>


Development Environment
=======================
If you want to patch or enhance this component, you will need to create a suitable development environment. The easiest way
to do that is to install phix4componentdev:

    # phix4componentdev
    sudo apt-get install php5-xdebug
    sudo apt-get install php5-imagick
    sudo pear channel-discover pear.phix-project.org
    sudo pear -D auto_discover=1 install -Ba phix/phix4componentdev

You can then clone the git repository:

    # ComponentName
    git clone https://bitbucket.org/bairwell/namealternatives.git

Then, install a local copy of this component's dependencies to complete the development environment:

    # build vendor/ folder
    phing build-vendor

To make life easier for you, common tasks (such as running unit tests, generating code review analytics, and creating the PEAR package) have been automated using [phing](http://phing.info).  You'll find the automated steps inside the build.xml file that ships with the component.

Run the command 'phing' in the component's top-level folder to see the full list of available automated tasks.


Licence
=======
This work is licensed under the MIT License

Copyright (c) 2011 [Bairwell Ltd - http://www.bairwell.com](ttp://www.bairwell.com)

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and
associated documentation files (the "Software"), to deal in the Software without restriction,
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or
substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Thanks
------
* [Carlton Northern](http://code.google.com/p/nickname-and-diminutive-names-lookup/) for:
  data which is used in
  NameAlternatives/GB/Forenames/carlton.csv

* [Anne Johnston](http://www.nireland.com/anne.johnston/) for:
  [http://www.nireland.com/anne.johnston/Diminutives.htm](http://www.nireland.com/anne.johnston/Diminutives.htm)
  which is used in
  NameAlternatives/GB/Forenames/nireland.csv

* [Francis Irving](http://www.Scraperwiki.com) for:
  http://ukparse.kforge.net/parlparse/
  which is used in
  NameAlternatives/GB/Forenames/parlimentaryparser.csv
  NameAlternatives/GB/Surnames/parlimentaryparser.csv

* [Geoff Allan](http://www.isle-of-wight-fhs.co.uk/) for:
  http://www.isle-of-wight-fhs.co.uk/bmd/surn.htm
  which is used in
  NameAlternatives/GB/Surnames/isleofwight.csv

* [Alex Coles](http://www.wing-ops.org.uk/) for:
  http://www.wing-ops.org.uk/spellings.html
  which is used in
  NameAlternatives/GB/Surnames/wingops.csv

* [Richard Chiswell](http://blog.rac.me.uk) of [Bairwell Web Development](http://www.bairwell.com) for:
  The main bulk of the code and his contributions in
  NameAlternatives/GB/Forenames/rchiswell.csv
  NameAlternatives/GB/PlaceNames/rchiswell.csv

* The contributors to Wikipedia for:
  http://en.wikipedia.org/wiki/Welsh_toponymy and many other pages
  which is used in
  NameAlternatives/GB/PlaceNames/wikipedia.csv
  NameAlternatives/GB/Surnames/wikipedia.csv

ChangeLog
=========
0.1 : First release